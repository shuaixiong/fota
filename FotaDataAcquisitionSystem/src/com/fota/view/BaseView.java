package com.fota.view;

import org.eclipse.swt.widgets.Shell;

public class BaseView
{
	public static void CentreWnd(Shell shell)
	{
		int width = shell.getMonitor().getClientArea().width;
		int height = shell.getMonitor().getClientArea().height;
		int x = shell.getSize().x;
		int y = shell.getSize().y;
		if (x > width)
		{
			shell.getSize().x = width;
		}
		if (y > height)
		{
			shell.getSize().y = height;
		}
		shell.setLocation((width - x+250) / 2, (height - y) / 2);
	}

	public static void left(Shell shell)
	{
		int width = shell.getMonitor().getClientArea().width;
		int height = shell.getMonitor().getClientArea().height;
		int x = shell.getSize().x;
		int y = shell.getSize().y;
		if (x > width)
		{
			shell.getSize().x = width;
		}
		if (y > height)
		{
			shell.getSize().y = height;
		}
		shell.setLocation(50, (height - y) / 2);
	}
}
