package com.fota.view;

import java.io.File;
import java.util.Hashtable;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.fota.util.CRHUtils;
import com.fota.util.Constant;
import com.fota.util.FotaUtil;
import com.fota.util.GZHUtils;
import com.fota.util.PropertyConstant;
import com.fota.util.PropertyReader;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class SettingDialog extends Dialog
{
	protected Object result;
	protected Shell shell;
	private Text text;
	private Text text_1;
	private Text text_2;
	private Properties properties;
	private AppMainView appMainView;
	private Label label_2;
	private Text text_3;
	private Label label_3;
	private Text text_4;
	private Text text_bundleNumber;
	private Text text_pkgNumber;
	private Text maxnum_text;
	private Label label_6;

	public SettingDialog(Shell parent, int style, Properties properties, AppMainView appMainView)
	{
		super(parent, style);
		this.properties = properties;
		this.appMainView = appMainView;
		setText("Setting Dialog");
	}

	public Object open()
	{
		createContents();
		this.shell.addShellListener(new ShellAdapter()
		{
			public void shellClosed(ShellEvent e)
			{
				Hashtable<String, String> htKeyValue = new Hashtable<>();
				htKeyValue.put(PropertyConstant.SERVERIP, text.getText());
				htKeyValue.put(PropertyConstant.SERVERPORT, text_1.getText());
				htKeyValue.put(PropertyConstant.LOCALHOSTPORT, text_2.getText());
				htKeyValue.put(PropertyConstant.USERNUMBER, text_3.getText());
				htKeyValue.put(PropertyConstant.FSNFILEPATH, text_4.getText());
				htKeyValue.put(PropertyConstant.BUNDLENUMBER, text_bundleNumber.getText());
				htKeyValue.put(PropertyConstant.PKGNUMBER, text_pkgNumber.getText());
				htKeyValue.put(PropertyConstant.MAXNUMBER, maxnum_text.getText());
				GZHUtils.PKGNUMBER = Integer.valueOf(text_pkgNumber.getText());
				GZHUtils.BUNDLENUMBER = Integer.valueOf(text_bundleNumber.getText());
				GZHUtils.MAXNUM = Integer.valueOf(maxnum_text.getText());
				CRHUtils.PKGNUMBER = Integer.valueOf(text_pkgNumber.getText());
				CRHUtils.BUNDLENUMBER = Integer.valueOf(text_bundleNumber.getText());
				CRHUtils.MAXNUM = Integer.valueOf(maxnum_text.getText());
				boolean flag = PropertyReader.setValueAndStore(Constant.configPath, htKeyValue);
			}

		});
		this.shell.open();
		this.shell.layout();
		BaseView.CentreWnd(this.shell);

		label_3 = new Label(shell, SWT.NONE);
		label_3.setBounds(10, 172, 82, 17);
		label_3.setText("数据储存位置：");
		text_4 = new Text(shell, SWT.BORDER);
		text_4.setBounds(119, 169, 222, 23);
		text_4.setText(properties.getProperty(PropertyConstant.FSNFILEPATH));
		text_4.setEditable(false);
		Button button = new Button(shell, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				// 点击事件 选择文件夹
				folderDig(shell);
			}
		});
		button.setBounds(347, 167, 80, 27);
		button.setText("选择");

		Label label_4 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_4.setBounds(40, 57, 387, 2);

		Label label = new Label(shell, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(320, 10, 6, 40);

		Label label_5 = new Label(shell, SWT.NONE);
		label_5.setBounds(95, 23, 62, 17);
		label_5.setText("张一捆");
		
		label_6 = new Label(shell, SWT.SEPARATOR);
		label_6.setBounds(164, 10, 6, 40);

		Display display = getParent().getDisplay();
		while (!this.shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		return this.result;
	}

	private void createContents()
	{
		this.shell = new Shell(getParent(), getStyle());
		this.shell.setToolTipText("请输入本机端口号");
		this.shell.setSize(519, 349);
		this.shell.setText("设置");
		Label label = new Label(shell, SWT.NONE);
		label.setBounds(255, 23, 62, 17);
		label.setText("捆一包");
		maxnum_text = new Text(shell, SWT.BORDER);
		maxnum_text.setBounds(19, 20, 73, 23);
		text_bundleNumber = new Text(shell, SWT.BORDER);
		text_bundleNumber.setBounds(176, 20, 73, 23);

		Label label_1 = new Label(shell, SWT.NONE);
		label_1.setText("包打二次包");
		label_1.setBounds(408, 23, 105, 17);

		text_pkgNumber = new Text(shell, SWT.BORDER);
		text_pkgNumber.setBounds(332, 20, 73, 23);
		label_2 = new Label(shell, SWT.NONE);
		label_2.setBounds(48, 117, 56, 17);
		label_2.setText("  员工号：");
		label_2.setVisible(false);
		text_3 = new Text(shell, SWT.BORDER);
		text_3.setBounds(130, 114, 220, 23);

		this.text_3.setText(properties.getProperty(PropertyConstant.USERNUMBER));
		text_3.addVerifyListener(new TextVerifyListener(2));
		text_3.setTextLimit(Constant.serialDatalen);
		Label lblip = new Label(this.shell, 0);
		lblip.setBounds(10, 117, 61, 17);
		lblip.setText("服务器IP：");
		text_3.setVisible(false);
		lblip.setVisible(false);
		this.text = new Text(this.shell, 2048);
		this.text.setToolTipText("请输入上级服务器IP地址");
		this.text.setBounds(130, 42, 220, 23);
		this.text.setText(properties.getProperty(PropertyConstant.SERVERIP));
		text.setVisible(false);
		Label label1 = new Label(this.shell, 0);
		label1.setBounds(22, 79, 80, 17);
		label1.setText("服务器端口号：");
		label1.setVisible(false);
		this.text_1 = new Text(this.shell, 2048);
		this.text_1.setToolTipText("请输入上级服务器端口号");
		this.text_1.setBounds(155, 71, 220, 23);
		this.text_1.setText(properties.getProperty(PropertyConstant.SERVERPORT));
		this.text_1.setVisible(false);
		Label label_2 = new Label(this.shell, 0);
		label_2.setText("本机端口号：");
		label_2.setBounds(22, 79, 80, 17);

		this.text_2 = new Text(this.shell, 2048);
		this.text_2.setToolTipText("请输入本机端口号");
		this.text_2.setBounds(119, 76, 220, 23);
		this.text_2.setText(properties.getProperty(PropertyConstant.LOCALHOSTPORT));

		Label lblip_1 = new Label(this.shell, 0);
		String ip = FotaUtil.getLocalIP();
		lblip_1.setText("本机的IP地址:" + ip);
		lblip_1.setBounds(95, 244, 220, 17);
		this.text_bundleNumber.setText(properties.getProperty(PropertyConstant.BUNDLENUMBER));
		this.text_pkgNumber.setText(properties.getProperty(PropertyConstant.PKGNUMBER));
		this.maxnum_text.setText(properties.getProperty(PropertyConstant.MAXNUMBER));
	}

	/**
	 * 文件选择对话框
	 */
	protected void fileDig(Shell parent)
	{
		// 新建文件对话框，并设置为打开的方式
		FileDialog filedlg = new FileDialog(parent, SWT.OPEN);
		// 设置文件对话框的标题
		filedlg.setText("文件选择");
		// 设置初始路径
		filedlg.setFilterPath("SystemRoot");
		// 打开文件对话框，返回选中文件的绝对路径
		String selected = filedlg.open();
		System.out.println("您选中的文件路径为：" + selected);
	}

	/**
	 * 文件夹（目录）选择对话框
	 */
	protected void folderDig(Shell parent)
	{
		// 新建文件夹（目录）对话框
		DirectoryDialog folderdlg = new DirectoryDialog(parent);
		// 设置文件对话框的标题
		folderdlg.setText("请选择存放FSN文件的位置");
		// 设置初始路径
		folderdlg.setFilterPath("SystemDrive");
		// 设置对话框提示文本信息
		folderdlg.setMessage("请选择相应的文件夹");
		// 打开文件对话框，返回选中文件夹目录
		String selecteddir = folderdlg.open();
		if (selecteddir == null)
		{
			return;
		} else
		{

			text_4.setText(selecteddir);
			System.out.println("您选中的文件夹目录为：" + selecteddir);
		}
	}
}
