package com.fota.view;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.fota.util.ZipUtils;

public class PackageDialog extends Dialog
{

	protected Object result;
	protected Shell shell;
	private DirectoryDialog srcSelectFilePath;
	private DirectoryDialog destSelectFilePath;
	private Text srcText;
	private Text destText;
	private Text bankCode;
	private Text packageCode;
	private Combo combo;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public PackageDialog(Shell parent, int style)
	{
		super(parent, style);
		setText("配钞");
		srcSelectFilePath = new DirectoryDialog(parent, style);
		destSelectFilePath = new DirectoryDialog(parent, style);
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open()
	{
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents()
	{
		shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 300);
		shell.setText(getText());

		BaseView.CentreWnd(shell);

		Label destPath = new Label(shell, 0);
		destPath.setBounds(48, 149, 61, 17);
		destPath.setText("打包路径：");

		srcText = new Text(shell, SWT.BORDER);
		srcText.setBounds(115, 94, 223, 23);

		destText = new Text(shell, SWT.BORDER);
		destText.setBounds(115, 146, 223, 23);

		Label srcPath = new Label(shell, 0);
		srcPath.setBounds(37, 97, 72, 20);
		srcPath.setText("源文件路径：");

		Button srcBtn = new Button(shell, SWT.NONE);
		srcBtn.setBounds(354, 94, 54, 23);
		srcBtn.setText("浏览");
		srcBtn.addMouseListener(new MouseListener()
		{

			@Override
			public void mouseUp(MouseEvent e)
			{
				srcSelectFilePath.open();
				if (null == srcSelectFilePath.getFilterPath())
					return;
				srcText.setText(srcSelectFilePath.getFilterPath().replace("\\", "/"));
			}

			@Override
			public void mouseDown(MouseEvent arg0)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDoubleClick(MouseEvent arg0)
			{
				// TODO Auto-generated method stub

			}
		});

		Button destBtn = new Button(shell, SWT.NONE);
		destBtn.setText("浏览");
		destBtn.setBounds(354, 146, 54, 23);
		destBtn.addMouseListener(new MouseListener()
		{

			@Override
			public void mouseUp(MouseEvent arg0)
			{
				destSelectFilePath.open();
				if (null == destSelectFilePath.getFilterPath())
					return;
				destText.setText(destSelectFilePath.getFilterPath().replace("\\", "/"));
			}

			@Override
			public void mouseDown(MouseEvent arg0)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDoubleClick(MouseEvent arg0)
			{
				// TODO Auto-generated method stub

			}
		});

		Button startPack = new Button(shell, SWT.NONE);
		startPack.setBounds(182, 209, 80, 27);
		startPack.setText("开始打包");

		Label bankCodeLabel = new Label(shell, SWT.NONE);
		bankCodeLabel.setBounds(25, 46, 84, 21);
		bankCodeLabel.setText("报送银行编码：");

		Label packageCodeLabel = new Label(shell, SWT.NONE);
		packageCodeLabel.setBounds(215, 46, 36, 20);
		packageCodeLabel.setText("包号：");

		bankCode = new Text(shell, SWT.BORDER);
		bankCode.setBounds(115, 44, 80, 23);
		bankCode.setFocus();
		bankCode.setTextLimit(6);
		bankCode.addVerifyListener(new VerifyListener()
		{

			@Override
			public void verifyText(VerifyEvent e)
			{
				boolean b = ("0123456789".indexOf(e.text) >= 0);
				e.doit = b;
			}
		});

		packageCode = new Text(shell, SWT.BORDER);
		packageCode.setBounds(258, 43, 80, 23);
		packageCode.setTextLimit(6);

		combo = new Combo(shell, SWT.NONE | SWT.READ_ONLY);
		combo.setBounds(346, 42, 88, 25);
		combo.add("一级打包");
		combo.add("二级打包");
		combo.select(0);

		packageCode.addVerifyListener(new TextVerifyListener(3));

		startPack.addMouseListener(new MouseListener()
		{

			@Override
			public void mouseUp(MouseEvent e)
			{
				String bnkCode = bankCode.getText();
				String pkgCode = packageCode.getText();
				String srcDir = srcText.getText();
				String destDir = destText.getText();
				int selectionIndex = combo.getSelectionIndex();

				if (bnkCode.length() != 6)
				{
					showDialog("请检查报送银行编码的长度!");
					bankCode.setFocus();
					bankCode.selectAll();
					return;
				}
				if (pkgCode.length() != 6)
				{
					showDialog("请检查包号的长度!");
					packageCode.setFocus();
					packageCode.selectAll();
					return;
				}
				if ("".equals(srcDir))
				{
					showDialog("请选择要打包文件的路径!");
					srcText.setFocus();
					srcText.selectAll();
					return;
				}
				if ("".equals(destDir))
				{
					showDialog("请选择要打包文件的输出路径!");
					destText.setFocus();
					destText.selectAll();
					return;
				}

				try
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
					String date = sdf.format(new Date());
					String fileName = null;
					File file = new File(srcDir);
					File destFile = new File(destDir);

					if (!file.exists())
					{
						showDialog("您选择的文件目录不存在，请重新选择！");
						srcText.setFocus();
						srcText.selectAll();
						return;
					}
					if (!destFile.exists())
					{
						showDialog("您选择的文件目录不存在，请重新选择！");
						destText.setFocus();
						destText.selectAll();
						return;
					}

					if (0 == selectionIndex)
						fileName = bnkCode + "_" + pkgCode + "_" + date + ".GZH";
					else if (1 == selectionIndex)
					{
						File[] files = file.listFiles();
						if (files.length > 0)
							fileName = bnkCode + "_" + files.length + "_" + date + ".GZH";
					}

					String zipFilePath = destDir + "/" + fileName;
					ZipUtils.zip(srcDir, zipFilePath);

					int choice = showDialog("打包完成！");
					System.out.println(choice);
					// if(choice == SWT.YES)
					shell.dispose();
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}

			}

			@Override
			public void mouseDown(MouseEvent arg0)
			{
			}

			@Override
			public void mouseDoubleClick(MouseEvent arg0)
			{
			}
		});

	}

	public int showDialog(String message)
	{
		// MessageBox box = new MessageBox( shell ,SWT.ICON_ERROR|SWT.YES|SWT.NO);
		MessageBox box = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES);
		// 设置对话框的标题
		box.setText("提示");
		// 设置对话框显示的消息
		box.setMessage(message);
		// 打开对话框，将返回值赋给choice
		int choice = box.open();
		// if (choice==SWT.YES)
		// System.out.print("Yes");
		// else if ( choice==SWT.NO)
		// System.out.print("No");
		return choice;
	}
}
