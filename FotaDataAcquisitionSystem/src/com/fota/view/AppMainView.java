package com.fota.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.wb.swt.SWTResourceManager;

import com.fota.service.CashManagerTCPListener;
import com.fota.service.client.ConnectServer;
import com.fota.service.client.ConnectServer2CRH;
import com.fota.util.BusiInfoFile;
import com.fota.util.CRHInfoFile;
import com.fota.util.CRHUtils;
import com.fota.util.Constant;
import com.fota.util.FSNInfoFile;
import com.fota.util.FotaUtil;
import com.fota.util.GZHUtils;
import com.fota.util.PropertyConstant;
import com.fota.util.PropertyReader;
import com.fota.util.TimeUtil;

import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Combo;

public class AppMainView implements SelectionListener
{
	protected Shell shlFota;
	private Text submittedBankCodeText;
	private MenuItem settingMenu;
	private Button endTask;
	private SettingDialog settingDialog = null;
	private List listView;
	private Properties properties;
	private Label userNumber;
	private MenuItem packMenu;
	private PackageDialog packageView;
	private Label label_1;
	private Combo businessType;
	private MenuItem disPlayNoteWordItemMenu;
	private BanknoteWordManagerView banknoteWordManagerView = null;
	private MenuItem item;

	public static void main(String[] args)
	{
		try
		{
			AppMainView window = new AppMainView();
			window.open();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void open()
	{
		Display display = Display.getDefault();
		createContents();
		banknoteWordManagerView = new BanknoteWordManagerView();
		this.shlFota.open();
		this.shlFota.layout();
		while (!this.shlFota.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}

	protected void createContents()
	{
		this.shlFota = new Shell(192);
		this.shlFota.addShellListener(new ShellAdapter()
		{
			public void shellClosed(ShellEvent e)
			{
				if (null != banknoteWordManagerView && null != banknoteWordManagerView.shell && !banknoteWordManagerView.shell.isDisposed() && banknoteWordManagerView.shell.isVisible())
					banknoteWordManagerView.shell.close();
				CashManagerTCPListener.stop();
			}
		});
		String path = "/res/img/AMCS.jpg";
		this.shlFota.setImage(SWTResourceManager.getImage(this.getClass(), path));
		this.shlFota.setSize(596, 461);
		this.shlFota.setText("深圳贝斯特 冠字号采集器 V1.0.0");
		BaseView.CentreWnd(this.shlFota);

		Menu menu = new Menu(this.shlFota, 2);
		menu.setOrientation(33550000);
		menu.setLocation(new Point(0, 0));
		this.shlFota.setMenuBar(menu);

		this.settingMenu = new MenuItem(menu, 0);
		this.settingMenu.setText("设置");
		this.settingMenu.addSelectionListener(this);

	/*	MenuItem banknotWordManager = new MenuItem(menu, SWT.CASCADE);
		banknotWordManager.setText("冠字号");
		Menu banknotWordManagerMenu = new Menu(shlFota, SWT.DROP_DOWN);
		banknotWordManager.setMenu(banknotWordManagerMenu);
		disPlayNoteWordItemMenu = new MenuItem(banknotWordManagerMenu, 0);
		disPlayNoteWordItemMenu.setText("显示冠字号");
		disPlayNoteWordItemMenu.addSelectionListener(this);*/
		// packMenu = new MenuItem(menu, SWT.NONE);
		// packMenu.setText("配钞");
		// packMenu.addSelectionListener(this);

		Label lblFota = new Label(this.shlFota, 0);
		lblFota.setFont(SWTResourceManager.getFont("仿宋", 13, 0));
		lblFota.setAlignment(16777216);
		lblFota.setBounds(131, 10, 314, 27);
		lblFota.setText("深圳贝斯特 冠字号采集器");

		Label label = new Label(this.shlFota, 0);
		label.setBounds(21, 58, 90, 17);
		label.setText("报送银行编号：");
		label.setVisible(true);
		this.submittedBankCodeText = new Text(this.shlFota, 2048);
		submittedBankCodeText.setVisible(true);
		this.submittedBankCodeText.setBounds(117, 55, 216, 23);
		// this.submittedBankCodeText.addVerifyListener(new TextVerifyListener(2));
		// this.text.setTextLimit(Constant.serialDatalen);
		this.submittedBankCodeText.setTextLimit(14);
		this.submittedBankCodeText.addListener(SWT.CHANGED, new Listener()
		{

			@Override
			public void handleEvent(Event arg0)
			{
				String code = submittedBankCodeText.getText();

				if (code != null && code.length() < 4)
				{
					String substring = code.substring(1);
					if (FotaUtil.isNumeric(substring))
					{

						Hashtable<String, String> htKeyValue = new Hashtable<>();
						String text = code;
						htKeyValue.put(PropertyConstant.SUBMITTEDBANKCODE, text);
						PropertyReader.setValueAndStore(Constant.configPath, htKeyValue);
						GZHUtils.SUBMITTEDBANKCODE = text;
						CRHUtils.SUBMITTEDBANKCODE = text;
					}
				}
			}
		});
		this.endTask = new Button(this.shlFota, 0);
		this.endTask.addSelectionListener(this);
		this.endTask.setBounds(351, 140, 80, 27);
		this.endTask.setText("上传数据");
		this.endTask.setVisible(false);
		this.listView = new List(this.shlFota, 2816);
		this.listView.setBounds(10, 144, 570, 259);
		Constant.setListView(listView);
		listView.addMouseListener(new MouseListener()
		{

			@Override
			public void mouseUp(MouseEvent arg0)
			{

			}

			@Override
			public void mouseDown(MouseEvent arg0)
			{
				Menu menu = new Menu(listView);
				listView.setMenu(menu);
				item = new MenuItem(menu, SWT.PUSH);
				item.setText("清除");
				item.addListener(SWT.Selection, new Listener()
				{
					public void handleEvent(Event event)
					{
						listView.removeAll();
					}
				});
			}

			@Override
			public void mouseDoubleClick(MouseEvent arg0)
			{

			}
		});
		Label userNumber = new Label(shlFota, SWT.NONE);
		setUserNumber(userNumber);
		userNumber.setBounds(552, 24, 28, 17);

		label_1 = new Label(shlFota, SWT.NONE);
		label_1.setBounds(21, 94, 87, 17);
		label_1.setText("现金清分中心：");
		label_1.setVisible(true);
		businessType = new Combo(shlFota, SWT.NONE | SWT.READ_ONLY);
		businessType.setBounds(117, 91, 41, 25);
		businessType.add("否");
		businessType.add("是");

		businessType.setVisible(true);

		Button pkg_button = new Button(shlFota, SWT.NONE);
		pkg_button.setBounds(390, 53, 80, 27);
		pkg_button.setText("手动打包");
		businessType.addListener(SWT.Selection, new Listener()
		{
			public void handleEvent(Event event)
			{
				final String type = businessType.getSelectionIndex() == 0 ? "F" : "T";
				Hashtable<String, String> htKeyValue = new Hashtable<>();
				htKeyValue.put(PropertyConstant.ISCLEARCENTER, type);
				PropertyReader.setValueAndStore(Constant.configPath, htKeyValue);
				GZHUtils.ISCLEARCENTER = type;
				CRHUtils.ISCLEARCENTER = type;
			}
		});
		initConfig();
		String property = properties.getProperty(PropertyConstant.ISCLEARCENTER);
		if (property.equals("F"))
		{
			businessType.select(0);
		} else
		{
			businessType.select(1);
		}
		if (null != properties)
		{
			this.settingDialog = new SettingDialog(this.shlFota, 32832, properties, this);
			// userNumber.setText("员工号：     " + properties.getProperty(PropertyConstant.USERNUMBER));
			submittedBankCodeText.setText(properties.getProperty(PropertyConstant.SUBMITTEDBANKCODE));
		} else
		{
			listView.add("初始化程序失败！！");
		}
		CRHUtils.residualData();
		GZHUtils.residualData();
		pkg_button.addSelectionListener(new SelectionListener()
		{

			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				GZHUtils.packGZH(true);
				CRHUtils.packCRH(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{

			}
		});
	}

	public void widgetDefaultSelected(SelectionEvent arg0)
	{
	}

	public void widgetSelected(SelectionEvent arg0)
	{
		String text = arg0.getSource().toString();
		if (text.equals(this.settingMenu.toString()))
		{
			this.settingDialog.open();
		} else if (text.equals(this.endTask.toString()))
		{
			final String data = this.submittedBankCodeText.getText();
			if (data.length() != Constant.serialDatalen)
			{
				FotaUtil.show("流水号长度有误，请检查！");
				this.submittedBankCodeText.setFocus();
				return;
			}

			// Display.getDefault().asyncExec(new Runnable()
			// {
			// public void run()
			// {
			//
			// }
			// });
			final String type = businessType.getSelectionIndex() == 0 ? "12" : "11";
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					// sendFile(data, type);
				}
			}).start();
			// this.submittedBankCodeText.setFocus();
		}
		// else if (text.equals(this.packMenu.toString()))
		// {
		// packageView = new PackageDialog(shlFota, 32832);
		// packageView.open();
		// }
		else if (text.equals(this.disPlayNoteWordItemMenu.toString()))
		{
			if (null == banknoteWordManagerView.shell)
			{
				banknoteWordManagerView.open();
			} else if (banknoteWordManagerView.shell.isDisposed())
			{
				banknoteWordManagerView.open();
			}

		}

	}

	private void initConfig()
	{

		// int compareDate = TimeUtil.compareDate(TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE), "2016-11-15");
		// if (compareDate == -1)
		// {
		FotaUtil.initFoder();
		properties = PropertyReader.getProperties(Constant.configPath);
		CashManagerTCPListener.start(Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.LOCALHOSTPORT)));
		PropertyConfigurator.configure(Constant.configLog);
		// } else
		// {
		// listView.add("软件已经过期！！！！");
		// }
		// FotaUtil.initCleanFile(Constant.uploadFilePath);
	}

	private synchronized void sendFile(String serialData, String businessTypeStr)
	{

		java.util.List<File> resultFileName = new ArrayList<>();
		java.util.List<File> renameFiles = new ArrayList<>();
		java.util.List<File> ergodic = FotaUtil.ergodic(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/Datas/", resultFileName);
		if (ergodic.size() == 0)
		{
			FotaUtil.show("没有接收到端机发送的文件！");
			return;
		}
		FotaUtil.show("正在发送文件....");
		for (File file : ergodic)
		{
			java.io.File renameFile = FotaUtil.renameFile(file.getParent(), file.getName(), serialData + "_" + properties.getProperty(PropertyConstant.USERNUMBER));
			renameFiles.add(renameFile);
			BusiInfoFile.changeBusinessType2File(renameFile.getAbsolutePath(), businessTypeStr);
		}

		FotaUtil.setCountDownLatch(renameFiles.size() / 2);
		int reCount = 0;
		File nameFile = renameFiles.get(0);
		String busiInfoName = nameFile.getParent() + "/" + nameFile.getName().split("\\.")[0] + ".txt";
		String fsninfoName = nameFile.getParent() + "/" + nameFile.getName().split("\\.")[0] + ".fsn";
		BusiInfoFile busInfoFile = new BusiInfoFile(busiInfoName);
		FSNInfoFile fsnInfoFile = new FSNInfoFile(fsninfoName);
		CRHInfoFile crhInfoFile = new CRHInfoFile();
		String terminal = fsnInfoFile.getTerminal();
		String terminalCode = fsnInfoFile.getTerminalCoder();
		String recordDate = fsnInfoFile.getRecordTime();
		String crhPath = properties.getProperty(PropertyConstant.FSNFILEPATH) + "/CRH/" + "123456_" + terminal.replace("\\/", "-") + "_" + terminalCode + "_" + recordDate + ".crh";
		crhInfoFile.createCRHFile(crhPath);
		String bt = busInfoFile.getBusinessType();
		String businesselatedInfo = "";
		String CRHBusinessType = "0";
		String CRHTerminalType = "0";
		if (bt.equals("11"))
		{
			CRHBusinessType = "1";
			businesselatedInfo = serialData;
		} else if (bt.equals("12"))
		{
			businesselatedInfo = serialData;
			CRHBusinessType = "2";
		} else if (bt.equals("09"))
		{
			CRHBusinessType = "3";
		}
		String busterminalType = busInfoFile.getTerminalType();
		// 按照需求，为01大型清分机、02中型清分机、03小型清分机、04一口半点钞机、05A类点钞
		// 0为未定义，1为清分机具，2为存取款一体机，3为点钞机，4为取款机，5为兑换机具，其他数值保留不用
		if (busterminalType.equals("01") || busterminalType.equals("02") || busterminalType.equals("03"))
		{
			CRHTerminalType = "1";
		} else if (busterminalType.equals("05"))
		{
			CRHTerminalType = "3";
		}
		String time = fsnInfoFile.getRecordTime();
		crhInfoFile.writerHeard(time, "123456", busInfoFile.getSubBranchCode(), CRHBusinessType, "0000", "T", "1", CRHTerminalType, fsnInfoFile.getTerminal(), fsnInfoFile.getTerminalCoder(),
				businesselatedInfo, "");
		busInfoFile.coleStream();
		for (File file : renameFiles)
		{
			String ext = FotaUtil.getExtName(file.getName(), '.');
			if (ext.equalsIgnoreCase(".fsn"))
			{
				String busiInfoFilename = file.getParent() + "/" + file.getName().split("\\.")[0] + ".txt";
				BusiInfoFile busInFileTemp = new BusiInfoFile(busiInfoFilename);
				int count = busInFileTemp.getRecordCount();
				reCount = reCount + count;
				for (int i = 0; i < count; i++)
				{
					BusiInfoFile readNextRecord = busInFileTemp.readNextRecord();
					String version = readNextRecord.getVersion();
					String curValue = readNextRecord.getCurValue();
					String CRHCurValu = crhInfoFile.getCRHCurValue(curValue);
					String CRHVersion = crhInfoFile.getCRHVersion(version);
					crhInfoFile.writerCRHRecord(time, readNextRecord.getBanknoteWordNumber(), CRHVersion, CRHCurValu);
				}
				busInFileTemp.coleStream();
				new Thread(new ConnectServer(properties.getProperty(PropertyConstant.SERVERIP), Integer.valueOf(properties.getProperty(PropertyConstant.SERVERPORT)), file.getAbsolutePath(),
						busiInfoFilename)).start();
			}
		}
		crhInfoFile.changeCount2File(crhPath, reCount);
		try
		{
			FotaUtil.getCountDownLatch().await();
			crhInfoFile.coleStream();
			new Thread(new ConnectServer2CRH(properties.getProperty(PropertyConstant.SERVERIP), Integer.valueOf(properties.getProperty(PropertyConstant.SERVERPORT)), crhPath)).start();
			FotaUtil.show("文件发送完毕！流水号：" + serialData);
			banknoteWordManagerView.clear();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public Label getUserNumber()
	{
		return userNumber;
	}

	public void setUserNumber(Label userNumber)
	{
		this.userNumber = userNumber;
	}
}
