package com.fota.view;

import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TableColumn;

import com.fota.util.Constant;

import org.eclipse.swt.widgets.Label;

public class BanknoteWordManagerView
{

	protected Shell shell;
	private Table table;
	private Label countabel;
	private Label doubtlabel;

	public void clear()
	{

		Display.getDefault().syncExec(new Runnable()
		{
			public void run()
			{
				if (null != table)
				{
					table.clearAll();
					TableItem[] items = table.getItems();
					for (int i = 0; i < items.length; i++)
					{
						TableItem tableItem = items[i];
						tableItem.dispose();

					}
					countabel.setText("点钞张数：0张");
					doubtlabel.setText("可疑币张数：0张");
					Constant.doubtCout = 0;
				}
			}
		});

	}

	public void open()
	{
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents()
	{
		shell = new Shell(SWT.CLOSE | SWT.MIN);
		shell.setSize(725, 547);
		this.shell.addShellListener(new ShellAdapter()
		{
			public void shellClosed(ShellEvent e)
			{
				table.clearAll();
				Constant.setTable(null);
				table=null;
			}

		});
		String path = "/res/img/AMCS.jpg";
		this.shell.setImage(SWTResourceManager.getImage(this.getClass(), path));
		this.shell.setText("冠字号");
		BaseView.left(this.shell);
		shell.setLayout(null);

		table = new Table(shell, SWT.MULTI | SWT.FULL_SELECTION);
		table.setBounds(0, 41, 719, 468);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		Constant.setTable(table);
		// 创建表头的字符串数组
		String[] tableHeader = { "点钞日期", "冠字号", "版本", "币种", "币值", "真假", "序号" };
		for (int i = 0; i < tableHeader.length; i++)
		{
			TableColumn tableColumn = new TableColumn(table, SWT.CENTER);
			if (i == 0)
			{
				tableColumn.setWidth(140);
			} else
			{
				tableColumn.setWidth(94);
			}
			tableColumn.setText(tableHeader[i]);
			// 设置表头可移动，默认为false
			tableColumn.setMoveable(true);
		}

		countabel = new Label(shell, SWT.NONE);
		countabel.setBounds(10, 7, 165, 17);
		countabel.setText("点钞张数：0张");
		Constant.setCountLabel(countabel);

		doubtlabel = new Label(shell, SWT.NONE);
		doubtlabel.setBounds(193, 7, 160, 17);
		doubtlabel.setText("可疑币张数：0张");
		Constant.setDoubtLabel(doubtlabel);
	}

	public Table getTable()
	{
		return table;
	}

	public void setTable(Table table)
	{
		this.table = table;
	}

	public static void main(String[] args)
	{
		new BanknoteWordManagerView().clear();
	}
}
