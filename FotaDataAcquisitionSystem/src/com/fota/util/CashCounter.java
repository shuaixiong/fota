package com.fota.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 现金计数器
 * 
 * @author hongqian_li
 * 
 */
public class CashCounter
{
	private static Map<String, Integer> count50 = new HashMap<String, Integer>();
	private static Map<String, Integer> count1 = new HashMap<String, Integer>();
	private static Map<String, Integer> count5 = new HashMap<String, Integer>();
	private static Map<String, Integer> count10 = new HashMap<String, Integer>();
	private static Map<String, Integer> count20 = new HashMap<String, Integer>();
	private static Map<String, Integer> count100 = new HashMap<String, Integer>();

	public static void setCountFile(String file)
	{
		count50.put("50", 0);
		count1.put("1", 0);
		count5.put("5", 0);
		count10.put("10", 0);
		count20.put("20", 0);
		count100.put("100", 0);
		BusiInfoFile busiInfoFile = new BusiInfoFile(file);
		if (null != busiInfoFile)
		{
			int recordCount = busiInfoFile.getRecordCount() > 5 ? 5 : busiInfoFile.getRecordCount();
			for (int i = 0; i < recordCount; i++)
			{
				BusiInfoFile readNextRecord = busiInfoFile.readNextRecord();
				String curValue = readNextRecord.getCurValue();
				if (curValue.equals("001"))
				{
					Integer integer = count1.get("1");
					integer = integer + 1;
					count1.put("1", integer);
				} else if (curValue.equals("005"))
				{
					Integer integer = count5.get("5");
					integer = integer + 1;
					count5.put("5", integer);
				} else if (curValue.equals("010"))
				{
					Integer integer = count10.get("10");
					integer = integer + 1;
					count10.put("10", integer);
				} else if (curValue.equals("020"))
				{
					Integer integer = count20.get("20");
					integer = integer + 1;
					count20.put("20", integer);
				} else if (curValue.equals("050"))
				{
					Integer integer = count50.get("50");
					integer = integer + 1;
					count50.put("50", integer);
				} else if (curValue.equals("100"))
				{
					Integer integer = count100.get("100");
					integer = integer + 1;
					count100.put("100", integer);
				}
			}
		}
	}

	public static String getMaxCash()
	{

		Map<String, Integer> maxMap = new HashMap<>();
		int max = 0;

		int c1 = count1.get("1");
		int c5 = count5.get("5");
		if (c1 > c5)
		{
			max = c1;
			maxMap.clear();
			maxMap.put("1", 1);
		} else
		{
			max = c5;
			maxMap.clear();
			maxMap.put("5", 1);
		}
		int c10 = count10.get("10");
		if (max < c10)
		{
			max = c10;
			maxMap.clear();
			maxMap.put("10", 1);
		}

		int c20 = count20.get("20");
		if (max < c20)
		{
			max = c20;
			maxMap.clear();
			maxMap.put("20", 1);
		}

		int c50 = count50.get("50");
		if (max < c50)
		{
			max = c50;
			maxMap.clear();
			maxMap.put("50", 1);
		}
		int c100 = count100.get("100");
		if (max < c100)
		{
			max = c100;
			maxMap.clear();
			maxMap.put("100", 1);
		}
		for (Map.Entry<String, Integer> entry : maxMap.entrySet())
		{
			return entry.getKey();
		}
		return "0";
	}
}
