package com.fota.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author hongqian_li
 * 
 */
public class GZHUtils
{

	public static int BUNDLENUMBER = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.BUNDLENUMBER));
	public static int PKGNUMBER = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.PKGNUMBER));
	public static String SUBMITTEDBANKCODE = PropertyReader.getValue(Constant.configPath, PropertyConstant.SUBMITTEDBANKCODE);
	public static String ISCLEARCENTER = PropertyReader.getValue(Constant.configPath, PropertyConstant.ISCLEARCENTER);

	private static ArrayList<String> GZHFILES = new ArrayList<>();
	private static ArrayList<String> GZHFILESTEMP = new ArrayList<>();

	private static ArrayList<String> TWICEGZHFILES = new ArrayList<>();
	private static ArrayList<String> TWINCEGZHFILESTEMP = new ArrayList<>();

	public static int MAXNUM = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.MAXNUMBER));

	private static int count = 0;

	private static File MAXFSN;

	private static boolean isCreateNewFile = true;
	private static String subBranchCode = "12345678901234";
	private static String preGZHHDFile;
	private static String preCashType = null;

	/**
	 * 组合FSN文件
	 */
	synchronized public static void mergerFSN(List<String> ps)
	{

		for (String string : ps)
		{
			String ext = StringHelper.getFileExtention(string);
			if (ext.equalsIgnoreCase("fsn"))
			{
				try
				{
					// isCreateNewFile=true创建新的大FSN文件
					if (isCreateNewFile)
					{

						File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
						String parent = pfile.getParent();
						MAXFSN = new File(parent + "\\" + "GZHTEMP\\1\\" + "0123456789" + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".fsn");
						MAXFSN.createNewFile();
						File fsnFile = new File(string);
						createGZHHerad(GZHFILES.size() + 1);
						if (fsnFile.exists())
						{
							InputStream inputStream = null;
							inputStream = new FileInputStream(fsnFile);
							FotaUtil.saveCashFile(MAXFSN, inputStream, true);
						}
						addGZHFile(MAXFSN.getAbsolutePath());
						if (count >= MAXNUM)
						{
							count = 0;
							isCreateNewFile = true;
						} else
						{
							isCreateNewFile = false;
						}
					} else
					{

						File fsnFile = new File(string);
						if (fsnFile.exists())
						{
							InputStream inputStream = null;
							inputStream = new FileInputStream(fsnFile);
							inputStream.read(new byte[32]);
							FotaUtil.saveCashFile(MAXFSN, inputStream, true);
							FotaUtil.changeCount2File(MAXFSN.getAbsolutePath(), count);
						}
						addGZHFile(MAXFSN.getAbsolutePath());
						if (count >= MAXNUM)
						{
							count = 0;
							isCreateNewFile = true;
						}
					}

				} catch (Exception e)
				{
					count = 0;
				}

			} else if (ext.equalsIgnoreCase("txt"))
			{
				CashCounter.setCountFile(string);
				String maxCash = CashCounter.getMaxCash();
				// 检查是不是同样类型钞票
				if (null != preCashType && !maxCash.equals(preCashType))
				{
					FotaUtil.show("发现新面值/币种");
					addGZHFile(MAXFSN.getAbsolutePath());
					packGZH(false);
				}
				preCashType = maxCash;
				BusiInfoFile busiInfoFile = new BusiInfoFile(string);
				int recordCount = busiInfoFile.getRecordCount();
				subBranchCode = busiInfoFile.getNetCode();
				CRHUtils.mergerCRH(string);
				GZHUtils.addCount(recordCount);
			}
		}

	}

	/**
	 * 添加一个GZH FSN文件
	 * 
	 * @param file
	 */
	public static void addGZHFile(String file)
	{
		if (null != file)
		{
			boolean contains = GZHFILES.contains(file);
			if (!contains)
			{
				GZHFILES.add(file);
			}
		}
		// 触发自动打包
		if (GZHFILES.size() == BUNDLENUMBER && count >= MAXNUM)
		{
			count = 0;
			FotaUtil.show("正在执行自动打包GZH文件");
			GZHFILESTEMP = GZHFILES;
			GZHFILES = new ArrayList<>();
			autoPackGZH();
		}

	}

	/**
	 * 添加一个GZH zip文件(二次打包)
	 * 
	 * @param file
	 */
	public static void addTwiceGZHFile(String file)
	{
		if (null != file)
		{
			TWICEGZHFILES.add(file);
		}
		// 触发自动打包
		if (TWICEGZHFILES.size() == PKGNUMBER)
		{
			FotaUtil.show("正在执行自动打GZH二次包文件");
			TWINCEGZHFILESTEMP = TWICEGZHFILES;
			TWICEGZHFILES = new ArrayList<>();
			autoTwicePackGZH();
		}

	}

	/**
	 * 自动打包（二次包）
	 */
	public static void autoTwicePackGZH()
	{

		try
		{
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "GZHTEMP\\2\\";
			for (String file : TWINCEGZHFILESTEMP)
			{
				FotaUtil.Copy(file, patch);
			}
			ZipUtils.zip(patch, parent + "\\GZH\\PKG2\\" + SUBMITTEDBANKCODE + "_" + PKGNUMBER + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip");
			FotaUtil.clearFiles(patch);
			FotaUtil.show("自动打GZH二次包完成");
			preGZHHDFile = null;
		} catch (Exception e)
		{
			FotaUtil.show("自动打GZH二次包出错！");
			e.printStackTrace();
		}
		TWINCEGZHFILESTEMP.clear();
	}

	/**
	 * 手动打包（二次包）
	 */
	public static void twicePackGZH(boolean isToast)
	{

		try
		{
			TWINCEGZHFILESTEMP = TWICEGZHFILES;
			TWICEGZHFILES = new ArrayList<>();
			if (TWINCEGZHFILESTEMP.size() == 0)
			{
				return;
			}
			if (isToast)
			{
				FotaUtil.show("手动打GZH二次包开始");
			}
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "GZHTEMP\\2\\";
			for (String file : TWINCEGZHFILESTEMP)
			{
				FotaUtil.Copy(file, patch);
			}
			ZipUtils.zip(patch, parent + "\\GZH\\PKG2\\" + SUBMITTEDBANKCODE + "_" + TWINCEGZHFILESTEMP.size() + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip");
			FotaUtil.clearFiles(patch);
			if (isToast)
			{
				FotaUtil.show("手动打GZH二次包完成");
			}
		} catch (Exception e)
		{
			if (isToast)
			{
				FotaUtil.show("手动打GZH二次包出错！");
			}
			e.printStackTrace();
		}
		TWINCEGZHFILESTEMP.clear();
	}

	/**
	 * 自动打包
	 */
	public static void autoPackGZH()
	{

		try
		{
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "GZHTEMP\\1\\";
			String patchFSN = parent + "\\" + "GZHTEMP\\FSN\\";
			File f = new File(patch);
			// createGZHHerad(f.list().length);

			for (String file : GZHFILESTEMP)
			{
				FotaUtil.Copy(file, patchFSN);
			}

			String zipFilePath = parent + "\\GZH\\" + SUBMITTEDBANKCODE + "_0123456789" + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip";
			ZipUtils.zip(patch, zipFilePath);
			FotaUtil.clearFiles(patch);
			FotaUtil.show("自动打包GZH完成");
			preGZHHDFile = null;
			addTwiceGZHFile(zipFilePath);
		} catch (Exception e)
		{
			FotaUtil.show("自动打包GZH文件出错！");
			e.printStackTrace();
		}
		GZHFILESTEMP.clear();
	}

	/**
	 * 手动打包
	 */
	public static void packGZH(boolean isToast)
	{

		try
		{

			GZHFILESTEMP = GZHFILES;
			GZHFILES = new ArrayList<>();
			if (GZHFILESTEMP.size() == 0)
			{
				twicePackGZH(isToast);
				return;
			}
			if (isToast)
			{
				FotaUtil.show("正在执行手动打包GZH文件");
			}
			// CRHUtils.packCRH();
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "GZHTEMP\\1\\";
			String patchFSN = parent + "\\" + "GZHTEMP\\FSN\\";
			File f = new File(patch);
			createGZHHerad(f.list().length - 1);

			for (String file : GZHFILESTEMP)
			{
				FotaUtil.Copy(file, patchFSN);
			}

			String zipFilePath = parent + "\\GZH\\" + SUBMITTEDBANKCODE + "_" + "0123456789" + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip";
			ZipUtils.zip(patch, zipFilePath);
			FotaUtil.clearFiles(patch);
			if (isToast)
			{
				FotaUtil.show("手动打包GZH完成");
			}
			count = 0;
			isCreateNewFile = true;
			preGZHHDFile = null;
			addTwiceGZHFile(zipFilePath);
			twicePackGZH(isToast);
		} catch (Exception e)
		{
			if (isToast)
			{
				FotaUtil.show("手动打包GZH出错！");
			}

			e.printStackTrace();
		}
		GZHFILESTEMP.clear();
	}

	/**
	 * 创建GZH头文件
	 * 
	 * @return
	 */
	private static void createGZHHerad(int fileNumber)
	{
		/*
		 * GZHInfoFIle gzhInfoFIle = new GZHInfoFIle(); gzhInfoFIle.setRecordDate(TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2));
		 * gzhInfoFIle.setSubmittedBankCode(PropertyReader.getValue(Constant.configPath, PropertyConstant.SUBMITTEDBANKCODE)); gzhInfoFIle.setSubBranchCode("1234567890");
		 * gzhInfoFIle.setBusinessType("3"); gzhInfoFIle.setRecordCount(GZHFILESTEMP.size() + ""); gzhInfoFIle.setIsClearCenter(PropertyReader.getValue(Constant.configPath,
		 * PropertyConstant.SUBMITTEDBANKCODE)); gzhInfoFIle.setPeoplesBankFileVersion("1"); gzhInfoFIle.setPkgNo("0123456789"); gzhInfoFIle.setCurType("CNY");
		 * gzhInfoFIle.setReservedField("          ");
		 */
		if (null != preGZHHDFile)
		{
			File f = new File(preGZHHDFile);
			f.delete();
		}
		FileOutputStream fop = null;
		File file = null;
		String content = TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ":" + SUBMITTEDBANKCODE + ":" + subBranchCode + ":" + "3" + ":" + fileNumber + ":" + ISCLEARCENTER + ":" + "1"
				+ ":" + "0123456789" + ":" + "CNY" + ":" + "0";
		try
		{
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			// 报送银行编码+“_”+包号+“_”+日期时间(YYYYMMDDHH24MISS)
			file = new File(parent + "\\" + "GZHTEMP\\1\\" + SUBMITTEDBANKCODE + "_" + "0123456789" + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".gzh");
			if (!file.exists())
			{
				file.createNewFile();
			}
			fop = new FileOutputStream(file);
			// get the content in bytes
			byte[] contentInBytes = content.getBytes();
			fop.write(contentInBytes);
			preGZHHDFile = file.getAbsolutePath();
			fop.flush();
			fop.close();
			file = null;
		} catch (Exception e)
		{

		} finally
		{
			try
			{
				if (fop != null)
				{
					fop.close();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}

	public static void addCount(int num)
	{
		count = count + num;

	}

	public static String getPreCashType()
	{
		return preCashType;
	}

	public static void setPreCashType(String preCashType)
	{
		GZHUtils.preCashType = preCashType;
	}

	public static void residualData()
	{
		File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
		String parent = pfile.getParent();
		String patch = parent + "\\" + "GZHTEMP\\1\\";
		String patchFSN = parent + "\\" + "GZHTEMP\\ResidualData\\";
		pfile=new File(patch);
		File[] listFiles = pfile.listFiles();
		for (int i = 0; i < listFiles.length; i++)
		{
			FotaUtil.Copy(listFiles[i].getAbsolutePath(), patchFSN);
		}
		FotaUtil.clearFiles(patch);
	}
}
