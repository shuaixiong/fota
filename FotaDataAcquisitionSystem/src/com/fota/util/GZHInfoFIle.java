package com.fota.util;

/**
 * GZH文件信息 GZH = ZIP=GZH头文件+FSN文件
 * 
 * 
 * 字 段 长 度 类型 说 明 (字节)
 * 
 * 该笔业务的日期，格式为 YYYYMMDDHH24MISS。 记录日期 14 字符串 24 时制的年(4 位)月(2 位)日(2 位)时(2 位) 分(2 位)秒(2 位)。 报送银行 报送单位的金融机构编码，银行编码和网点编 14 字符串 码统一使用人民银行总行调查统计司发布的 编码 《金融机构编码规范》中规定的金融机构编码 生成网点 14 字符串 清分中心记录冠字号码的，是管辖清分中心的
 * 编码 银行分支机构的“金融机构编码” 业务类型 1 字符串 业务类型值固定位 3，表示清分业务。跨行调 款应全额清分，其他数值禁止使用。 记录数 3 字符串 本 GZH 文件包含的 FSN 文件数，用于校验文件 的完整性。 是否现金 1 字符串 如果是则为“T”,否则为“F” 清分中心 版本格式 1 字符串 文件版本，1 表示当前的 1.0 版本。 包（袋、 10 字符串 与物流相关的
 * RFID 数据，用于识别款包 RFID 箱）号 信息。 币种标志 4 字符串 最多 4 位大写 ASCII 英文字母。 保留字段 10 - 保留
 * 
 * @author hongqian_li
 * 
 */
public class GZHInfoFIle
{

	/**
	 * 该笔业务的日期 14字节
	 */
	private String recordDate;

	/**
	 * 报送银行 报送单位的金融机构编码，银行编码和网点编 14个字节
	 */
	private String submittedBankCode;

	/**
	 * 生成网点号14字节 网点记录冠字号码的，是生成冠字号码记录的网点“金融机构编码”； 清分中心记录冠字号码的，是管辖清分中心的银行分支机构的“金融机构编码”
	 */
	private String subBranchCode;

	/**
	 * 业务类型1字节 业务类型值固定位 3，表示清分业务。跨行调款应全额清分，其他数值禁止使用。
	 */
	private String businessType;

	/**
	 * 记录数3字节 本文件记录数，用于校验文件完整性
	 */
	private String recordCount;

	/**
	 * 是否现金清分中心 1 字节 如果是则为“T”,否则为“F
	 */
	private String isClearCenter;

	/**
	 * 人民银行系统格式版本 1字节 文件版本，1表示当前的1.0版本。
	 */
	private String PeoplesBankFileVersion;

	/**
	 * 包（袋、箱）号10 字符串 与物流相关的 RFID 数据，用于识别款包 RFID信息。
	 */
	private String pkgNo;

	/**
	 * 最多 4 位大写 ASCII 英文字母。 最多 4 位大写 ASCII 英文字母。
	 */
	private String curType;// CNY:人民币
	/**
	 * 备用10字节
	 */
	private String reservedField;

	public String getRecordDate()
	{
		return recordDate;
	}

	public void setRecordDate(String recordDate)
	{
		this.recordDate = recordDate;
	}

	public String getSubmittedBankCode()
	{
		return submittedBankCode;
	}

	public void setSubmittedBankCode(String submittedBankCode)
	{
		this.submittedBankCode = submittedBankCode;
	}

	public String getSubBranchCode()
	{
		return subBranchCode;
	}

	public void setSubBranchCode(String subBranchCode)
	{
		this.subBranchCode = subBranchCode;
	}

	public String getBusinessType()
	{
		return businessType;
	}

	public void setBusinessType(String businessType)
	{
		this.businessType = businessType;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getIsClearCenter()
	{
		return isClearCenter;
	}

	public void setIsClearCenter(String isClearCenter)
	{
		this.isClearCenter = isClearCenter;
	}

	public String getPeoplesBankFileVersion()
	{
		return PeoplesBankFileVersion;
	}

	public void setPeoplesBankFileVersion(String peoplesBankFileVersion)
	{
		PeoplesBankFileVersion = peoplesBankFileVersion;
	}

	public String getPkgNo()
	{
		return pkgNo;
	}

	public void setPkgNo(String pkgNo)
	{
		this.pkgNo = pkgNo;
	}

	public String getCurType()
	{
		return curType;
	}

	public void setCurType(String curType)
	{
		this.curType = curType;
	}

	public String getReservedField()
	{
		return reservedField;
	}

	public void setReservedField(String reservedField)
	{
		this.reservedField = reservedField;
	}

	@Override
	public String toString()
	{
		return "GZHInfoFIle [recordDate=" + recordDate + ", submittedBankCode=" + submittedBankCode + ", subBranchCode=" + subBranchCode + ", businessType=" + businessType + ", recordCount="
				+ recordCount + ", isClearCenter=" + isClearCenter + ", PeoplesBankFileVersion=" + PeoplesBankFileVersion + ", pkgNo=" + pkgNo + ", curType=" + curType + ", reservedField="
				+ reservedField + "]";
	}

}
