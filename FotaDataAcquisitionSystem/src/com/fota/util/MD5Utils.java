package com.fota.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

/**
 * MD5
 * 
 * @author hongqian_li
 */
public class MD5Utils
{
	public static String getMd5ByFile(String file)
	{
		try
		{
			FileInputStream fis = new FileInputStream(file);
			String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(fis));
			IOUtils.closeQuietly(fis);
			return md5;
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return null;
	}

	public static String getMd5ByFile(InputStream file)
	{
		try
		{
			String md5 = DigestUtils.md5Hex(IOUtils.toByteArray(file));
			IOUtils.closeQuietly(file);
			return md5;
		} catch (Exception e)
		{
			System.out.println(e.toString());
		}
		return null;
	}

	/**
	 * 比较2个文件MD5
	 * 
	 * @param file1
	 * @param file2
	 * @return
	 */
	public static boolean compareFileMid(String file1, InputStream file2)
	{
		String md5ByFile1 = getMd5ByFile(file1);
		if (null == md5ByFile1)
		{
			return false;
		}
		String md5ByFile2 = getMd5ByFile(file2);
		if (null == md5ByFile2)
		{
			return false;
		}
		if (md5ByFile1.equals(md5ByFile2))
		{
			return true;
		}
		return false;
	}

	/**
	 * 比较2个文件MD5
	 * 
	 * @param file1
	 * @param file2
	 * @return
	 */
	public static boolean compareFileMid(String file1, String file2)
	{
		String md5ByFile1 = getMd5ByFile(file1);
		if (null == md5ByFile1)
		{
			return false;
		}
		String md5ByFile2 = getMd5ByFile(file2);
		if (null == md5ByFile2)
		{
			return false;
		}
		if (md5ByFile1.equals(md5ByFile2))
		{
			return true;
		}
		return false;
	}

	public static void main(String[] args) throws IOException
	{

		String path = "D:\\data\\test.fsn";

		String md5ByFile = getMd5ByFile(path);
		System.out.println("MD5:" + md5ByFile.toUpperCase());

		// System.out.println("MD5:"+DigestUtils.md5Hex("WANGQIUYUN"));
	}
}
