package com.fota.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import org.apache.log4j.Logger;

public class CRHInfoFile
{

	static Logger logger = Logger.getLogger(CRHInfoFile.class);

	private OutputStream CRHOutputStream;

	// 文件头100字节

	/**
	 * 记录日期 2字节 该笔业务的日期，参考《数据格式》(2)中Date的算法定义。
	 */

	private String recordDate;

	/**
	 * 报送银行编号 6字节 报送单位的金融机构编码，银行编码和网点编码统一使用人民银行总行调查统计司发布的《金融机构编码规范》中规定的金融机构编码
	 * */
	private String SubmittedBankCode;

	/**
	 * 生成网点号6字节 网点记录冠字号码的，是生成冠字号码记录的网点“金融机构编码”； 清分中心记录冠字号码的，是管辖清分中心的银行分支机构的“金融机构编码”
	 */
	private String subBranchCode;

	/**
	 * 业务类型1字节 未定义为0 现金收入为1 现金付出为2 清分业务为3 其他数值禁止使用
	 */
	private String businessType;

	/**
	 * 记录数4字节 本文件记录数，用于校验文件完整性
	 */
	private String recordCount;
	/**
	 * 是否现金清分中心 1 字节 如果是则为“T”,否则为“F
	 */
	private String isClearCenter;

	/**
	 * 人民银行系统格式版本 1字节 文件版本，1表示当前的1.0版本。
	 */
	private String PeoplesBankFileVersion;

	/**
	 * 设备类别 1字节 记录冠字号码的设备类别：0为未定义，1为清分机具，2为存取款一体机，3为点钞机，4为取款机，5为兑换机具，其他数值保留不用
	 */
	private String terminalType;

	/**
	 * 机型 8字节 来源于《数据格式》中的“机型编号”中的“机型”，使用厂商既定的机型命名规则。
	 */
	private String terminal;

	/**
	 * 设备编号 10字节 来源于《数据格式》中的“机型编号”中的“编号”，为该机具的产品序列号。
	 */
	private String terminalCode;

	/**
	 * 业务关联信息 50字节 当业务类型为1或2时，该字段填写业务办理人账号信息或身份证号码或业务流水号；当业务类型为0或3时，该字段填0。
	 */
	private String BusinesselatedInfo;

	/**
	 * 备用10字节
	 */
	private String ReservedField;

	// 冠字号部分 16字节

	/**
	 * 记录时间 2 字节 点一把钞时，机器开始点验钞的时间，参考《数据格式》中Time的算法定义。
	 */
	private String recordTime;

	/**
	 * 12位字母加数字组合，不能识别的可用下划线号“_”代替
	 */
	private String bankNoteWordNumber;

	/**
	 * 版别 1 0表示1990版，1表示第五套1999版，2表示2005版，3~254保留,255表示其它币种（不考虑年版或无法识别）。
	 */
	private String version;

	/**
	 * 币值 1 纸币面值，0表示无法识别，1表示1元，2表示5元，3表示10元，4表示20元，5表示50元，6表示100元，7~255保留。
	 */
	private String curValue;

	public CRHInfoFile()
	{
	}

	/**
	 * @param recordDate
	 *            记录日期 2字节
	 * @param SubmittedBankCode
	 *            报送银行编号 6字节
	 * @param subBranchCode
	 *            生成网点号6字节
	 * @param businessType
	 *            业务类型 未定义为0 现金收入为1 现金付出为2 清分业务为3 其他数值禁止使用
	 * @param recordCount
	 *            记录数4字节
	 * @param isClearCenter
	 *            是否现金清分中心1字节 如果是则为“T”,否则为“F”
	 * @param PeoplesBankFileVersion
	 *            人民银行系统格式版本 1字节 文件版本，1表示当前的1.0版本。
	 * @param terminalType
	 *            设备类别 1字节 记录冠字号码的设备类别：0为未定义，1为清分机具，2为存取款一体机，3为点钞机，4为取款机，5为兑换机具，其他数值保留不用
	 * @param terminal
	 *            机型 8字节 来源于《数据格式》中的“机型编号”中的“机型”，使用厂商既定的机型命名规则。
	 * @param terminalCode
	 *            设备编号 10字节 来源于《数据格式》中的“机型编号”中的“编号”，为该机具的产品序列号。
	 * @param BusinesselatedInfo
	 *            业务关联信息 50字节 当业务类型为1或2时，该字段填写业务办理人账号信息或身份证号码或业务流水号；当业务类型为0或3时，该字段填0。
	 * @param ReservedField
	 *            保留字段10字节 保留
	 * @return
	 */
	public boolean writerHeard(String recordDate, String SubmittedBankCode, String subBranchCode, String businessType, String recordCount, String isClearCenter, String PeoplesBankFileVersion,
			String terminalType, String terminal, String terminalCode, String BusinesselatedInfo, String ReservedField)
	{
		if (null != CRHOutputStream)
			try
			{
				CRHOutputStream.write(ByteHelper.getDateGM(recordDate));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(6, SubmittedBankCode));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(6, subBranchCode.substring(8, 12)));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, businessType));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(4, recordCount));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, isClearCenter));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, PeoplesBankFileVersion));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, terminalType));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(8, terminal));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(10, terminalCode));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(50, BusinesselatedInfo));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(10, ReservedField));
				return true;
			} catch (IOException e)
			{
				e.printStackTrace();
				logger.info("CRH写入头文件出错" + e.toString());
				return false;
			}
		return false;

	}

	public boolean writerCRHRecord(String recordTime, String bankNoteWordNumber, String version, String curValue)
	{
		if (null != CRHOutputStream)
			try
			{
				CRHOutputStream.write(ByteHelper.getTimeGM(recordTime));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, ","));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(12, bankNoteWordNumber));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, ","));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, version));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, ","));
				CRHOutputStream.write(ByteHelper.getFixedLengthByteArray(1, curValue));
				byte[] lineFeed = new byte[] { (byte) 0x0D, (byte) 0x0A };
				CRHOutputStream.write(lineFeed);
				return true;
			} catch (IOException e)
			{
				e.printStackTrace();
				logger.info("CRH写入冠字号文件出错" + e.toString());
				return false;
			}
		return false;
	}

	public boolean createCRHFile(String filePath)
	{
		try
		{
			File file = new File(filePath);
			CRHOutputStream = new FileOutputStream(file);
		} catch (Exception e)
		{
			coleStream();
			return false;
		}
		return true;
	}

	public String getRecordDate()
	{
		return recordDate;
	}

	public void setRecordDate(String recordDate)
	{
		this.recordDate = recordDate;
	}

	public String getSubmittedBankCode()
	{
		return SubmittedBankCode;
	}

	public void setSubmittedBankCode(String submittedBankCode)
	{
		SubmittedBankCode = submittedBankCode;
	}

	public String getSubBranchCode()
	{
		return subBranchCode;
	}

	public void setSubBranchCode(String subBranchCode)
	{
		this.subBranchCode = subBranchCode;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getBusinessType()
	{
		return businessType;
	}

	public void setBusinessType(String businessType)
	{
		this.businessType = businessType;
	}

	public String getIsClearCenter()
	{
		return isClearCenter;
	}

	public void setIsClearCenter(String isClearCenter)
	{
		this.isClearCenter = isClearCenter;
	}

	public String getPeoplesBankFileVersion()
	{
		return PeoplesBankFileVersion;
	}

	public void setPeoplesBankFileVersion(String peoplesBankFileVersion)
	{
		PeoplesBankFileVersion = peoplesBankFileVersion;
	}

	public String getTerminalType()
	{
		return terminalType;
	}

	public void setTerminalType(String terminalType)
	{
		this.terminalType = terminalType;
	}

	public String getTerminal()
	{
		return terminal;
	}

	public void setTerminal(String terminal)
	{
		this.terminal = terminal;
	}

	public String getTerminalCode()
	{
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode)
	{
		this.terminalCode = terminalCode;
	}

	public String getBusinesselatedInfo()
	{
		return BusinesselatedInfo;
	}

	public void setBusinesselatedInfo(String businesselatedInfo)
	{
		BusinesselatedInfo = businesselatedInfo;
	}

	public String getReservedField()
	{
		return ReservedField;
	}

	public void setReservedField(String reservedField)
	{
		ReservedField = reservedField;
	}

	public String getRecordTime()
	{
		return recordTime;
	}

	public void setRecordTime(String recordTime)
	{
		this.recordTime = recordTime;
	}

	public String getBankNoteWordNumber()
	{
		return bankNoteWordNumber;
	}

	public void setBankNoteWordNumber(String bankNoteWordNumber)
	{
		this.bankNoteWordNumber = bankNoteWordNumber;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getCurValue()
	{
		return curValue;
	}

	public void setCurValue(String curValue)
	{
		this.curValue = curValue;
	}

	public void coleStream()
	{
		if (null != CRHOutputStream)
			try
			{
				CRHOutputStream.close();
			} catch (Exception e)
			{
				e.printStackTrace();
			}
	}

	public static void main(String[] args)
	{
		String fsnPath = "E:\\CMG\\DataCreator\\data\\20150622153523\\20150622_000002.fsn";
		String txtPath = "E:\\CMG\\DataCreator\\data\\20150622153523\\20150622_000002.txt";
		String crhPath = "E:\\CMG\\DataCreator\\data\\20150622153523\\20150622_000002.crh";
		BusiInfoFile busInfoFile = new BusiInfoFile(txtPath);
		FSNInfoFile fsnInfoFile = new FSNInfoFile(fsnPath);
		int count = busInfoFile.getRecordCount();
		CRHInfoFile crhInfoFile = new CRHInfoFile();
		crhInfoFile.createCRHFile(crhPath);
		String bt = busInfoFile.getBusinessType();
		String businesselatedInfo = "";
		String CRHBusinessType = "0";
		String CRHTerminalType = "0";
		if (bt.equals("11"))
		{
			CRHBusinessType = "1";
			businesselatedInfo = "123456";
		} else if (bt.equals("12"))
		{
			businesselatedInfo = "123456";
			CRHBusinessType = "2";
		} else if (bt.equals("09"))
		{
			CRHBusinessType = "3";
		}
		String busterminalType = busInfoFile.getTerminalType();
		// 按照需求，为01大型清分机、02中型清分机、03小型清分机、04一口半点钞机、05A类点钞
		// 0为未定义，1为清分机具，2为存取款一体机，3为点钞机，4为取款机，5为兑换机具，其他数值保留不用
		if (busterminalType.equals("01") || busterminalType.equals("02") || busterminalType.equals("03"))
		{
			CRHTerminalType = "1";
		} else if (busterminalType.equals("05"))
		{
			CRHTerminalType = "3";
		}
		String time = fsnInfoFile.getRecordTime();
		crhInfoFile.writerHeard(time, "123456", busInfoFile.getSubBranchCode(), CRHBusinessType, "0000", "T", "1", CRHTerminalType, fsnInfoFile.getTerminal(), fsnInfoFile.getTerminalCoder(),
				businesselatedInfo, "");
		for (int i = 0; i < count; i++)
		{
			BusiInfoFile readNextRecord = busInfoFile.readNextRecord();
			String version = readNextRecord.getVersion();
			String curValue = readNextRecord.getCurValue();
			String CRHCurValu = getCRHCurValue(curValue);
			String CRHVersion = getCRHVersion(version);
			crhInfoFile.writerCRHRecord(time, readNextRecord.getBanknoteWordNumber(), CRHVersion, CRHCurValu);
		}
		busInfoFile.coleStream();
		
	}

	public static String getCRHCurValue(String curValue)
	{
		// 纸币面值，0表示无法识别，1表示1元，2表示5元，3表示10元，4表示20元，5表示50元，6表示100元，7~255保留。
		String CRHCurValu = "0";

		if (curValue.equals("001"))
		{
			CRHCurValu = "1";
		} else if (curValue.equals("005"))
		{
			CRHCurValu = "2";
		} else if (curValue.equals("010"))
		{
			CRHCurValu = "3";
		} else if (curValue.equals("020"))
		{
			CRHCurValu = "4";
		} else if (curValue.equals("050"))
		{
			CRHCurValu = "5";
		} else if (curValue.equals("100"))
		{
			CRHCurValu = "6";
		}
		return CRHCurValu;
	}

	public static String getCRHVersion(String version)
	{
		String CRHVersion = "";
		if (version.equals("1990"))
		{
			CRHVersion = "0";

		} else if (version.equals("1999"))
		{
			CRHVersion = "1";
		} else if (version.equals("2005"))
		{
			CRHVersion = "2";
		}else if(version.equals("2015"))
		{
			CRHVersion = "4";
		}
		return CRHVersion;
	}
	
	public  void changeCount2File(String path,int count)
	{
		RandomAccessFile raf = null;
		try
		{
			raf = new RandomAccessFile(path, "rw");
			raf.seek(15);
			raf.write(ByteHelper.int4byte(count));
			System.out.println();
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			if (null != raf)
				try
				{
					raf.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
		}
	}
}
