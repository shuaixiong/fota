package com.fota.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import org.apache.log4j.Logger;

/*
 * 交易信息：一个批次只有一个，占业务信息前60字节
 * 字段	长度	说明
 机具编号	24	地区号4位，支行号4位，网点号4位，厂家编号2位，机器编号是10位，如不足10位，前面补0，厂家编号2位由于A类点钞机厂商仍未出台，暂全部设置为00
 业务类型 	2	01取款出钞、02存款点钞、03存款回收、04取款废钞、05取款回收、06取款自动复位、07存款自动复位、09清分、10配钞、11柜面支付、12柜面存入、99其它
 其中11、12为点钞机使用，如果点钞机不支持或柜员没选择，默认使用11                                    
 设备类型 	2	 按照需求，为01大型清分机、02中型清分机、03小型清分机、04一口半点钞机、05A类点钞机、06存取款一体机、07取款机、99其他机具                                    
 卡号   	19	ATMC上送；无内容补空格占位                                                                        
 交易代码 	5	ATMC上送；无内容补空格占位；00001取款交易、00002存款交易、00003现金圈存交易                                                                       
 端机流水号	6	ATMC上送；无内容补空格占位
 人行业务类型	2	QK：取款机取款业务；CQ：存取款一体机取款业务；GM：柜面取款业务；如非取款业务，则上送空格

 */

public class BusiInfoFile
{
	// terminal_code,business_type,terminal_type,card,transaction_code,terminal_serial_number,people_bank_type
	public static final String FIELD_TERMINAL_CODE = "terminal_code";
	public static final String FIELD_DISTRICT_CODE = "district_code";
	public static final String FIELD_BRANCH_CODE = "branch_code";
	public static final String FIELD_SUBBRANCH_CODE = "subbranch_code";
	public static final String FIELD_COMPANY_CODE = "company_code";
	public static final String FIELD_BUSINESS_TYPE = "business_type";
	public static final String FIELD_TERMINAL_TYPE = "terminal_type";
	public static final String FIELD_CARD = "card";
	public static final String FIELD_TRANSACTION_CODE = "transaction_code";
	public static final String FIELD_TERMINAL_SERIAL_NUMBER = "terminal_serial_number";
	public static final String FIELD_PEOPLE_BANK_TYPE = "people_bank_type";

	private int recordCount;
	private InputStream busInfoInputStream;

	static Logger logger = Logger.getLogger(BusiInfoFile.class);
	// 文件头共60个字节
	private String terminalCode;// 机具号
	private String districtCode;//地区号(4)
	private String branchCode;// 支行号(4)
	private String subBranchCode;// 网点号
	private String companyCode;// 公司编号
	private String businessType;// 交易类型
	private String terminalType;// 机具编号
	private String cardNo;
	private String transactionCode;
	private String terminalSerialNumber;
	private String peoleBankBusinessType;// 人行交易类型
	
	private String netCode;
	
	// 冠字号部分共52字节
	private String recordTime;// 记录时间 14字节 记录时间,年月日时分秒（YYYYMMDDhhmmss）
	private String banknoteWordNumber;// 冠字号码10字节 10位冠字号码（识别不出的某位可用下划线“_”代替）
	private String curType;// 币种3字节 CNY:人民币
	private String curValue;// 币值3字节 纸币面值，不足三位前端补零，如100，050
	private String version;// 版本4字节 1990、1999、2005 (9999表示无法识别)
	private String stats;// 0：假；1：真
	private String Condition;// 成色 2字节 01-99（00表示无法识别，99表示新旧程度接近原封新券）； ATM统一上送80
	private String serialNumber;// 序号5字节 同影像文件命名的序号；不足5位前补0
	private String ReservedField;// 备用10字节 备用字符，无内容补空格

	public BusiInfoFile(String FilePaht)
	{
		try
		{

			File file = new File(FilePaht);
			if (file.exists())
			{
				long length = file.length();
				long l = (length - 60) % 52;
				if (l == 0)
				{
					recordCount = (int) ((length - 60) / 52);
					busInfoInputStream = new FileInputStream(file);
					readHeardStream(busInfoInputStream);
				}
			}

		} catch (Exception e)
		{
			logger.info("解析BusIinfo文件出错：" + e);
			coleStream();
		}
	}

	public BusiInfoFile(InputStream FilePaht)
	{
		try
		{

			long length = FilePaht.available();
			long l = (length - 60) % 52;
			if (l == 0)
			{
				recordCount = (int) ((length - 60) / 52);
				busInfoInputStream = FilePaht;
				readHeardStream(busInfoInputStream);
			}

		} catch (Exception e)
		{
			logger.info("解析BusIinfo文件出错：" + e);
			coleStream();
		}
	}

	public boolean readHeardStream(InputStream inputStream)
	{
		byte buf[] = new byte[60];
		try
		{
			inputStream.read(buf);
			districtCode = StringHelper.byteArrayToString(buf, 0, 4);
			netCode=districtCode;
			branchCode = StringHelper.byteArrayToString(buf, 4, 4);
			netCode=netCode+branchCode;
			subBranchCode = StringHelper.byteArrayToString(buf, 8, 4);
			netCode=netCode+subBranchCode;
			companyCode = StringHelper.byteArrayToString(buf, 12, 2);
			netCode=netCode+companyCode;
			terminalCode = StringHelper.byteArrayToString(buf, 14, 10);
			businessType = StringHelper.byteArrayToString(buf, 24, 2);
			terminalType = StringHelper.byteArrayToString(buf, 26, 2);
			cardNo = StringHelper.byteArrayToString(buf, 28, 19);
			transactionCode = StringHelper.byteArrayToString(buf, 47, 5);
			terminalSerialNumber = StringHelper.byteArrayToString(buf, 52, 6);
			peoleBankBusinessType = StringHelper.byteArrayToString(buf, 58, 2);
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
			logger.error("Exception:", e);
			coleStream();
		}

		return false;
	}

	public BusiInfoFile readNextRecord()
	{
		if (null != busInfoInputStream)
		{
			byte[] data = new byte[52];
			try
			{
				busInfoInputStream.read(data);
				recordTime = StringHelper.byteArrayToString(data, 0, 14);
				banknoteWordNumber = StringHelper.byteArrayToString(data, 14, 10);
				curType = StringHelper.byteArrayToString(data, 24, 3);
				curValue = StringHelper.byteArrayToString(data, 27, 3);
				version = StringHelper.byteArrayToString(data, 30, 4);
				stats = StringHelper.byteArrayToString(data, 34, 1);
				Condition = StringHelper.byteArrayToString(data, 35, 2);
				serialNumber = StringHelper.byteArrayToString(data, 37, 5);
				ReservedField = StringHelper.byteArrayToString(data, 42, 10);
			} catch (IOException e)
			{
				e.printStackTrace();
			}

			return this;
		}
		return null;
	}

	// 指定年月日的日期数据的产生算法为： Date = ((Year-1980)<<9) + (Month<<5) + Day
	// 其中：Year为年份，大于等于1980；Month为月份；Day为日；
	// 指定时分秒的时间数据产生算法为：Time = (Hour<<11) + (Minute<<5) + (Second>>1)
	// 其中：0≤Hour < 24，0≤Minute < 60，0≤Second < 60
	// public String getDateTime() {
	// int date = Byte2IntHelper.UInt16ValueOfLittleEndian(recordBuf, POS_DATE);
	// int time = Byte2IntHelper.UInt16ValueOfLittleEndian(recordBuf, POS_TIME);
	// int year = (date >> 9) + 1980;
	// int month = date >> 5 & 0x0F;
	// int day = date & 0x1F;
	// int hrs = time >> 11;
	// int min = time >> 5 & 0x3F;
	// int sec = time << 1 & 0x3F;
	//
	// Calendar calendar = new GregorianCalendar(year, month - 1, day, hrs,
	// min, sec);
	// SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
	// return format.format(calendar.getTime());
	// }

	/*
	 * 记录时间 14 记录时间,年月日时分秒（YYYYMMDDhhmmss） ，冠字号码采集时间 冠字号码 10 10位冠字号码（识别不出的某位可用下划线“_”代替） 币种 3 CNY:人民币 币值 3 纸币面值，不足三位前端补零，如100，050 版别 4 1990、1999、2005 (9999表示无法识别) 真假 1 0：假；1：真 成色 2
	 * 01-99（00表示无法识别，99表示新旧程度接近原封新券）； ATM统一上送80 序号 5 同影像文件命名的序号；不足5位前补0 备用 10 备用字符，无内容补空格
	 */
	public boolean writeOneRecord(OutputStream outputStream)
	{
		return true;
	}

	public boolean writeToStream(OutputStream outputStream)
	{
		try
		{
			outputStream.write(districtCode.getBytes());
			outputStream.write(branchCode.getBytes());
			outputStream.write(subBranchCode.getBytes());
			outputStream.write(companyCode.getBytes());
			outputStream.write(terminalCode.getBytes());
			outputStream.write(businessType.getBytes());
			outputStream.write(terminalType.getBytes());
			outputStream.write(cardNo.getBytes());
			outputStream.write(transactionCode.getBytes());
			outputStream.write(terminalSerialNumber.getBytes());
			outputStream.write(peoleBankBusinessType.getBytes());
			return true;
		} catch (IOException e)
		{
			logger.error("Exception:", e);
			e.printStackTrace();
		}
		return false;
	}

	public static void changeBusinessType2File(String path, String BusinessType)
	{
		RandomAccessFile raf = null;
		try
		{
			raf = new RandomAccessFile(path, "rw");
			raf.seek(24);
			raf.write(BusinessType.getBytes());
			System.out.println();
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			if (null != raf)
				try
				{
					raf.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
		}
	}

	public boolean isValid()
	{
		return true;
	}

	public String getFieldValue(String field)
	{
		if (field.equals(FIELD_DISTRICT_CODE))
		{
			return getDistrictCode();
		} else if (field.equals(FIELD_BRANCH_CODE))
		{
			return getBranchCode();
		} else if (field.equals(FIELD_SUBBRANCH_CODE))
		{
			return getSubBranchCode();
		} else if (field.equals(FIELD_COMPANY_CODE))
		{
			return getCompanyCode();
		} else if (field.equals(FIELD_TERMINAL_CODE))
		{
			return getTerminalCode();
		} else if (field.equals(FIELD_BUSINESS_TYPE))
		{
			return getBusinessType();
		} else if (field.equals(FIELD_TERMINAL_TYPE))
		{
			return getTerminalType();
		} else if (field.equals(FIELD_CARD))
		{
			return getCardNo();
		} else if (field.equals(FIELD_TRANSACTION_CODE))
		{
			return getTransactionCode();
		} else if (field.equals(FIELD_TERMINAL_SERIAL_NUMBER))
		{
			return getTerminalSerialNumber();
		} else if (field.equals(FIELD_PEOPLE_BANK_TYPE))
		{
			return getPeoleBankBusinessType();
		} else
		{
			return "";
		}
	}

	public String getTerminalCode()
	{
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode)
	{
		this.terminalCode = terminalCode;
	}

	public String getBusinessType()
	{
		return businessType;
	}

	public void setBusinessType(String businessType)
	{
		this.businessType = businessType;
	}

	public String getTerminalType()
	{
		return terminalType;
	}

	public void setTerminalType(String terminalType)
	{
		this.terminalType = terminalType;
	}

	public String getCardNo()
	{
		return cardNo;
	}

	public void setCardNo(String cardNo)
	{
		this.cardNo = cardNo;
	}

	public String getTransactionCode()
	{
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode)
	{
		this.transactionCode = transactionCode;
	}

	public String getTerminalSerialNumber()
	{
		return terminalSerialNumber;
	}

	public void setTerminalSerialNumber(String terminalSerialNumber)
	{
		this.terminalSerialNumber = terminalSerialNumber;
	}

	public String getPeoleBankBusinessType()
	{
		return peoleBankBusinessType;
	}

	public void setPeoleBankBusinessType(String peoleBankBusinessType)
	{
		this.peoleBankBusinessType = peoleBankBusinessType;
	}

	public String getDistrictCode()
	{
		return districtCode;
	}

	public void setDistrictCode(String districtCode)
	{
		this.districtCode = districtCode;
	}

	public String getBranchCode()
	{
		return branchCode;
	}

	public void setBranchCode(String branchCode)
	{
		this.branchCode = branchCode;
	}

	public String getSubBranchCode()
	{
		return subBranchCode;
	}

	public void setSubBranchCode(String subBranchCode)
	{
		this.subBranchCode = subBranchCode;
	}

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	public int getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(int recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getRecordTime()
	{
		return recordTime;
	}

	public void setRecordTime(String recordTime)
	{
		this.recordTime = recordTime;
	}

	public String getBanknoteWordNumber()
	{
		return banknoteWordNumber;
	}

	public void setBanknoteWordNumber(String banknoteWordNumber)
	{
		this.banknoteWordNumber = banknoteWordNumber;
	}

	public String getCurType()
	{
		return curType;
	}

	public void setCurType(String curType)
	{
		this.curType = curType;
	}

	public String getCurValue()
	{
		return curValue;
	}

	public void setCurValue(String curValue)
	{
		this.curValue = curValue;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getCondition()
	{
		return Condition;
	}

	public void setCondition(String condition)
	{
		Condition = condition;
	}

	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getReservedField()
	{
		return ReservedField;
	}

	public void setReservedField(String reservedField)
	{
		ReservedField = reservedField;
	}

	public String getStats()
	{
		return stats;
	}

	public void setStats(String stats)
	{
		this.stats = stats;
	}

	public static void main(String[] args)
	{
		//hangeBusinessType2File("E:/CMG/DataCreator/data/test/4.txt", "11");
		BusiInfoFile busiInfoFile = new BusiInfoFile("D:\\data\\test.TXT");
		int recordCount2 = busiInfoFile.getRecordCount();
		BusiInfoFile readNextRecord = busiInfoFile.readNextRecord();
		System.out.println(readNextRecord);
	}

	@Override
	public String toString()
	{
		return "BusiInfoFile [recordCount=" + recordCount + ", terminalCode=" + terminalCode + ", districtCode=" + districtCode + ", branchCode=" + branchCode + ", subBranchCode=" + subBranchCode
				+ ", companyCode=" + companyCode + ", businessType=" + businessType + ", terminalType=" + terminalType + ", cardNo=" + cardNo + ", transactionCode=" + transactionCode
				+ ", terminalSerialNumber=" + terminalSerialNumber + ", peoleBankBusinessType=" + peoleBankBusinessType + ", recordTime=" + recordTime + ", banknoteWordNumber=" + banknoteWordNumber
				+ ", curType=" + curType + ", curValue=" + curValue + ", version=" + version + ", stats=" + stats + ", Condition=" + Condition + ", serialNumber=" + serialNumber + ", ReservedField="
				+ ReservedField + "]";
	}

	public void coleStream()
	{
		if (null != busInfoInputStream)
			try
			{
				busInfoInputStream.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public String getNetCode()
	{
		return netCode;
	}

	public void setNetCode(String netCode)
	{
		this.netCode = netCode;
	}
	
}
