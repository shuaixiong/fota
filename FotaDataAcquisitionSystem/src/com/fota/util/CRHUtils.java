package com.fota.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * 
 * @author hongqian_li
 * 
 */
public class CRHUtils
{

	public static int BUNDLENUMBER = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.BUNDLENUMBER));
	public static int PKGNUMBER = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.PKGNUMBER));
	public static String SUBMITTEDBANKCODE = PropertyReader.getValue(Constant.configPath, PropertyConstant.SUBMITTEDBANKCODE);
	public static String ISCLEARCENTER = PropertyReader.getValue(Constant.configPath, PropertyConstant.ISCLEARCENTER);

	private static ArrayList<String> CRHFILES = new ArrayList<>();
	private static ArrayList<String> CRHFILESTEMP = new ArrayList<>();

	private static ArrayList<String> TWICECRHFILES = new ArrayList<>();
	private static ArrayList<String> TWINCECRHFILESTEMP = new ArrayList<>();

	private static File MAXCRH;

	public static int MAXNUM = Integer.valueOf(PropertyReader.getValue(Constant.configPath, PropertyConstant.MAXNUMBER));

	private static int count = 0;

	private static boolean isCreateNewFile = true;
	private static String preCashType = null;

	/**
	 * 机器编号
	 */
	private String terminalType;

	/**
	 * 添加一个CRH txt文件
	 * 
	 * @param file
	 */
	public static void addCRHFile(String file)
	{
		if (null != file)
		{
			CRHFILES.add(file);
		}
		// 触发自动打包
		if (CRHFILES.size() == BUNDLENUMBER * PKGNUMBER)
		{
			FotaUtil.show("正在执行自动打包CRH文件");
			CRHFILESTEMP = CRHFILES;
			CRHFILES = new ArrayList<>();
			autoPackCRH();
		}

	}

	/**
	 * 添加一个CRH zip文件(二次打包)
	 * 
	 * @param file
	 */
	public static void addTwiceCRHFile(String file)
	{
		if (null != file)
		{
			TWICECRHFILES.add(file);
		}
		// 触发自动打包
		if (TWICECRHFILES.size() == PKGNUMBER)
		{
			FotaUtil.show("正在执行自动打CRH二次包文件");
			TWINCECRHFILESTEMP = TWICECRHFILES;
			TWICECRHFILES = new ArrayList<>();
			autoTwicePackCRH();
		}

	}

	/**
	 * 自动打包（二次包）
	 */
	public static void autoTwicePackCRH()
	{

		try
		{
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "CRHTEMP\\2\\";
			for (String file : TWINCECRHFILESTEMP)
			{
				FotaUtil.Copy(file, patch);
			}
			ZipUtils.zip(patch, parent + "\\CRH\\" + SUBMITTEDBANKCODE + "_" + PKGNUMBER + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip");
			FotaUtil.clearFiles(patch);
			FotaUtil.show("自动打CRH二次包完成");
		} catch (Exception e)
		{
			FotaUtil.show("自动打CRH二次包出错！");
			e.printStackTrace();
		}
		TWINCECRHFILESTEMP.clear();
	}

	/**
	 * 自动打包
	 */
	public static void autoPackCRH()
	{

		try
		{
			FotaUtil.show("正在执行自动打包CRH");
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "CRHTEMP\\1\\";
			String patchCRH = parent + "\\" + "CRHTEMP\\CRH\\";
			for (String file : CRHFILESTEMP)
			{
				FotaUtil.Copy(file, patchCRH);
			}
			String zipFilePath = parent + "\\CRH\\" + SUBMITTEDBANKCODE + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip";
			ZipUtils.zip(patch, zipFilePath);
			FotaUtil.clearFiles(patch);
			FotaUtil.show("自动打包CRH完成");
			// addTwiceCRHFile(zipFilePath);
			isCreateNewFile = true;
		} catch (Exception e)
		{
			FotaUtil.show("自动打包CRH文件出错！");
			e.printStackTrace();
		}
		CRHFILESTEMP.clear();
	}

	/**
	 * 手动打包
	 */
	public static void packCRH(boolean isToast)
	{

		try
		{
			isCreateNewFile = true;
			CRHFILESTEMP = CRHFILES;
			CRHFILES = new ArrayList<>();
			if (isToast)
			{
				FotaUtil.show("正在执行手动打包CRH文件");
			}
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			String patch = parent + "\\" + "CRHTEMP\\1\\";
			String patchCRH = parent + "\\" + "CRHTEMP\\CRH\\";
			// createCRHerad();

			for (String file : CRHFILESTEMP)
			{
				FotaUtil.Copy(file, patchCRH);
			}

			String zipFilePath = parent + "\\CRH\\" + SUBMITTEDBANKCODE + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".zip";
			ZipUtils.zip(patch, zipFilePath);
			FotaUtil.clearFiles(patch);
			if (isToast)
			{
				FotaUtil.show("手动打包CRH完成");
			}
			// addTwiceCRHFile(zipFilePath);
		} catch (Exception e)
		{
			if (isToast)
			{
				FotaUtil.show("手动打包CRH出错！");
			}
			e.printStackTrace();
		}
		CRHFILESTEMP.clear();
	}

	public static void residualData()
	{
		File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
		String parent = pfile.getParent();
		String patch = parent + "\\" + "CRHTEMP\\1\\";
		String patchCRH = parent + "\\" + "CRHTEMP\\ResidualData\\";
		pfile=new File(patch);
		File[] listFiles = pfile.listFiles();
		for (int i = 0; i < listFiles.length; i++)
		{
			FotaUtil.Copy(listFiles[i].getAbsolutePath(), patchCRH);
		}
		FotaUtil.clearFiles(patch);
	}

	/**
	 * 创建CRH头文件
	 * 
	 * @return
	 */
	private static void createCRHerad(String filep)
	{

		/*
		 * // 日期 byte[] dateGM = ByteHelper.getTimeGM(TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2)); byte[] byteMerger = ByteHelper.byteMerger(new byte[] {}, dateGM); // 报送银行编号 byte[]
		 * submittedbankcodeByte = ByteHelper.toByteArray(SUBMITTEDBANKCODE, 6); byteMerger = ByteHelper.byteMerger(byteMerger, submittedbankcodeByte);
		 * 
		 * // 机型 String model = ""; // 设备编号 String terminalCode = ""; String fileStr = filep; BusiInfoFile busiInfoFile = new BusiInfoFile(fileStr); if (null != busiInfoFile) { // 网点号 String
		 * subBranchCode = StringHelper.addOperator(busiInfoFile.getSubBranchCode(), '0', 6, true); byte[] subBranchCodeByte = subBranchCode.getBytes(); byteMerger = ByteHelper.byteMerger(byteMerger,
		 * subBranchCodeByte); // //业务类型 "10"-ATM配钞,"11"-取款,"12"-取款 String businessType = busiInfoFile.getBusinessType(); byte[] businessTypeByte = new byte[] { 0x30 }; byte[] businessRelatedInfo =
		 * new byte[] { 0x30 }; String cardNo = busiInfoFile.getCardNo(); // TODO 业务类型有冲突 if ("10".equals(businessType)) { businessTypeByte = new byte[] { 0x31 }; businessRelatedInfo =
		 * cardNo.getBytes(); } else if ("11".equals(businessType)) { businessTypeByte = new byte[] { 0x32 }; businessRelatedInfo = cardNo.getBytes(); } else if ("12".equals(businessType)) {
		 * businessTypeByte = new byte[] { 0x33 }; } byteMerger = ByteHelper.byteMerger(byteMerger, businessTypeByte); // 文件个数 byte[] number = ByteHelper.int2Bytes(CRHFILESTEMP.size()); byteMerger =
		 * ByteHelper.byteMerger(byteMerger, number); // 是否是清分 byte[] isCByte = ISCLEARCENTER.getBytes(); byteMerger = ByteHelper.byteMerger(byteMerger, isCByte); // 版本号 byte[] peploVersionByte = new
		 * byte[] { 0x31 }; byteMerger = ByteHelper.byteMerger(byteMerger, peploVersionByte); // 设备类别 byte[] terminalType = new byte[] { 0x31 }; terminalCode = busiInfoFile.getTerminalCode();
		 * byteMerger = ByteHelper.byteMerger(byteMerger, terminalType); // 机型 model = terminalCode.substring(0, 5); StringHelper.addOperator(model, '_', 8, true); byte[] modelByte =
		 * StringHelper.addOperator(model, '_', 8, true).getBytes(); byteMerger = ByteHelper.byteMerger(byteMerger, modelByte); // 机器编号 byte[] terminalCodeByte = StringHelper.addOperator(terminalCode,
		 * '_', 10, true).getBytes(); byteMerger = ByteHelper.byteMerger(byteMerger, terminalCodeByte); byteMerger = ByteHelper.byteMerger(byteMerger, StringHelper.addOperator(new
		 * String(businessRelatedInfo), '_', 50, true).getBytes()); byteMerger = ByteHelper.byteMerger(byteMerger,new byte[]{(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte)
		 * 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF,(byte) 0xFF} );
		 */
		// 日期
		byte[] dateGM = ByteHelper.getTimeGM(TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2));
		byte[] byteMerger = ByteHelper.byteMerger(new byte[] {}, dateGM);
		// 报送银行编号
		String snmStr = SUBMITTEDBANKCODE.substring(1);
		byte[] submittedbankcodeByte = ByteHelper.longToByte(Long.valueOf(snmStr));
		byteMerger = ByteHelper.byteMerger(byteMerger, submittedbankcodeByte);

		// 机型
		String model = "";
		// 设备编号
		String terminalCode = "";
		String fileStr = filep;
		BusiInfoFile busiInfoFile = new BusiInfoFile(fileStr);
		if (null != busiInfoFile)
		{
			// 网点号
			// String subBranchCode = StringHelper.addOperator(busiInfoFile.getSubBranchCode(), '0', 6, true);
			byte[] subBranchCodeByte = ByteHelper.longToByte(Long.valueOf(busiInfoFile.getNetCode()));
			;
			byteMerger = ByteHelper.byteMerger(byteMerger, subBranchCodeByte);
			// //业务类型 "10"-ATM配钞,"11"-取款,"12"-取款
			String businessType = busiInfoFile.getBusinessType();
			byte[] businessTypeByte = new byte[] { 0x00 };
			byte[] businessRelatedInfo = new byte[] { 0x30 };
			String cardNo = busiInfoFile.getCardNo();
			// TODO 业务类型有冲突
			if ("10".equals(businessType))
			{
				businessTypeByte = new byte[] { 0x01 };
				businessRelatedInfo = cardNo.getBytes();
			} else if ("11".equals(businessType))
			{
				businessTypeByte = new byte[] { 0x02 };
				businessRelatedInfo = cardNo.getBytes();
			} else if ("12".equals(businessType))
			{
				businessTypeByte = new byte[] { 0x03 };
			}
			byteMerger = ByteHelper.byteMerger(byteMerger, businessTypeByte);
			// 文件个数
			byte[] number = ByteHelper.int2Bytes(CRHFILESTEMP.size());
			byteMerger = ByteHelper.byteMerger(byteMerger, number);
			// 是否是清分
			byte[] isCByte = ISCLEARCENTER.getBytes();
			byteMerger = ByteHelper.byteMerger(byteMerger, isCByte);
			// 版本号
			byte[] peploVersionByte = new byte[] { 0x01 };
			byteMerger = ByteHelper.byteMerger(byteMerger, peploVersionByte);
			// 设备类别
			byte[] terminalType = new byte[] { 0x01 };
			terminalCode = busiInfoFile.getTerminalCode();
			byteMerger = ByteHelper.byteMerger(byteMerger, terminalType);
			// 机型
			model = terminalCode.substring(0, 5);
			StringHelper.addOperator(model, '_', 8, true);
			byte[] modelByte = StringHelper.addOperator(model, '_', 8, true).getBytes();
			byteMerger = ByteHelper.byteMerger(byteMerger, modelByte);
			// 机器编号
			byte[] terminalCodeByte = StringHelper.addOperator(terminalCode, '_', 10, true).getBytes();
			byteMerger = ByteHelper.byteMerger(byteMerger, terminalCodeByte);
			byteMerger = ByteHelper.byteMerger(byteMerger, StringHelper.addOperator(new String(businessRelatedInfo), '_', 50, true).getBytes());
			byteMerger = ByteHelper.byteMerger(byteMerger, new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
					(byte) 0xFF });
		}

		FileOutputStream fop = null;
		try
		{
			File pfile = new File(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH));
			String parent = pfile.getParent();
			// 报送银行编码+“_”+机型+_“设备编号“_”+日期时间(YYYYMMDDHH24MISS)
			MAXCRH = new File(parent + "\\" + "CRHTEMP\\1\\" + SUBMITTEDBANKCODE + "_" + model + "_" + terminalCode + "_" + TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + ".crh");
			MAXCRH.createNewFile();
			fop = new FileOutputStream(MAXCRH);
			// get the content in bytes
			fop.write(byteMerger);
			int recordCount = busiInfoFile.getRecordCount();
			for (int i = 0; i < recordCount; i++)
			{
				BusiInfoFile readNextRecord = busiInfoFile.readNextRecord();
				// 日期
				String recordTime = readNextRecord.getRecordTime();
				byte[] dateGM2 = ByteHelper.getTimeGM(recordTime);
				byte[] allByte = ByteHelper.byteMerger(new byte[] {}, dateGM2);
				allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
				String banknoteWordNumber = StringHelper.addOperator(readNextRecord.getBanknoteWordNumber(), '_', 12, true);
				allByte = ByteHelper.byteMerger(allByte, banknoteWordNumber.getBytes());
				allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
				// 版本号
				String version = readNextRecord.getVersion();
				byte[] vType = new byte[] { (byte) 0x255 };
				if (version.equals("1990"))
				{
					vType = new byte[] { (byte) 0x00 };
				} else if (version.equals("1999"))
				{
					vType = new byte[] { (byte) 0x01 };
				} else if (version.equals("2005"))
				{
					vType = new byte[] { (byte) 0x02 };
				} else if (version.equals("2015"))
				{
					vType = new byte[] { (byte) 0x03 };
				}
				allByte = ByteHelper.byteMerger(allByte, vType);
				allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
				// 币值
				String curValue = readNextRecord.getCurValue();
				byte[] curType = new byte[] { (byte) 0x00 };
				if (curValue.equals("001"))
				{
					curType = new byte[] { (byte) 0x01 };
				} else if (curValue.equals("005"))
				{
					curType = new byte[] { (byte) 0x02 };
				} else if (curValue.equals("010"))
				{
					curType = new byte[] { (byte) 0x03 };
				} else if (curValue.equals("020"))
				{
					curType = new byte[] { (byte) 0x04 };
				} else if (curValue.equals("050"))
				{
					curType = new byte[] { (byte) 0x05 };
				} else if (curValue.equals("100"))
				{
					curType = new byte[] { (byte) 0x06 };
				}
				allByte = ByteHelper.byteMerger(allByte, curType);
				allByte = ByteHelper.byteMerger(allByte, new byte[] { 0x0D, 0x0A });
				fop.write(allByte);

			}
			FotaUtil.changeCRHCount2File(MAXCRH.getAbsolutePath(), count);
			fop.flush();
			fop.close();
		} catch (Exception e)
		{

		} finally
		{
			try
			{
				if (fop != null)
				{
					fop.close();
				}
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}

	/**
	 * 组合CRH文件
	 */
	synchronized public static void mergerCRH(String filep)
	{
		try
		{

			String maxCash = GZHUtils.getPreCashType();
			// 检查是不是同样类型钞票
			if (null != preCashType && !maxCash.equals(preCashType))
			{
				addCRHFile(MAXCRH.getAbsolutePath());
				packCRH(false);
			}
			preCashType = maxCash;

			if (isCreateNewFile)
			{
				isCreateNewFile = false;
				// 创建文件
				createCRHerad(filep);
			} else
			{
				BusiInfoFile busiInfoFile = new BusiInfoFile(filep);
				int recordCount = busiInfoFile.getRecordCount();
				FileOutputStream fileOutputStream = new FileOutputStream(MAXCRH, true);
				for (int i = 0; i < recordCount; i++)
				{
					BusiInfoFile readNextRecord = busiInfoFile.readNextRecord();
					// 日期
					String recordTime = readNextRecord.getRecordTime();
					byte[] dateGM2 = ByteHelper.getTimeGM(recordTime);
					byte[] allByte = ByteHelper.byteMerger(new byte[] {}, dateGM2);
					allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
					String banknoteWordNumber = StringHelper.addOperator(readNextRecord.getBanknoteWordNumber(), '_', 12, true);
					allByte = ByteHelper.byteMerger(allByte, banknoteWordNumber.getBytes());
					allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
					// 版本号
					String version = readNextRecord.getVersion();
					byte[] vType = new byte[] { (byte) 0x255 };
					if (version.equals("1990"))
					{
						vType = new byte[] { (byte) 0x00 };
					} else if (version.equals("1999"))
					{
						vType = new byte[] { (byte) 0x01 };
					} else if (version.equals("2005"))
					{
						vType = new byte[] { (byte) 0x02 };
					} else if (version.equals("2015"))
					{
						vType = new byte[] { (byte) 0x03 };
					}
					allByte = ByteHelper.byteMerger(allByte, vType);
					allByte = ByteHelper.byteMerger(allByte, ",".getBytes());
					// 币值
					String curValue = readNextRecord.getCurValue();
					byte[] curType = new byte[] { (byte) 0x00 };
					if (curValue.equals("001"))
					{
						curType = new byte[] { (byte) 0x01 };
					} else if (curValue.equals("005"))
					{
						curType = new byte[] { (byte) 0x02 };
					} else if (curValue.equals("010"))
					{
						curType = new byte[] { (byte) 0x03 };
					} else if (curValue.equals("020"))
					{
						curType = new byte[] { (byte) 0x04 };
					} else if (curValue.equals("050"))
					{
						curType = new byte[] { (byte) 0x05 };
					} else if (curValue.equals("100"))
					{
						curType = new byte[] { (byte) 0x06 };
					}
					allByte = ByteHelper.byteMerger(allByte, curType);
					allByte = ByteHelper.byteMerger(allByte, new byte[] { 0x0D, 0x0A });
					fileOutputStream.write(allByte);

				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}

			BusiInfoFile busiInfoFile = new BusiInfoFile(filep);
			int recordCount = busiInfoFile.getRecordCount();
			count = count + recordCount;
			FotaUtil.changeCRHCount2File(MAXCRH.getAbsolutePath(), count);
			if (count >= MAXNUM)
			{
				addCRHFile(MAXCRH.getAbsolutePath());
				count = 0;
				isCreateNewFile = true;
			}

		} catch (Exception e)
		{
			count = 0;
		}
	}

	public static boolean isCreateNewFile()
	{
		return isCreateNewFile;
	}

	public static void setCreateNewFile(boolean isCreateNewFile)
	{
		CRHUtils.isCreateNewFile = isCreateNewFile;
	}

	public static void main(String[] args)
	{
		String snmStr = "C0000000000000".substring(1);
		System.out.println(snmStr);
	}
}
