package com.fota.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FSNInfoFile
{
	private String serial_number;
	private String recordTime;
	private String terminal;
	private String terminalCoder;

	public FSNInfoFile(String path)
	{
		File fsnFile = new File(path);
		if (fsnFile.exists())
		{
			InputStream inputStream = null;
			try
			{
				inputStream = new FileInputStream(fsnFile);
				FileHeader f=new FileHeader();
				f.readFromStream(inputStream);
				/*inputStream.read(new byte[32]);
				byte[] data = new byte[100];
				inputStream.read(data);
				serial_number = StringHelper.uint16ByteLittleEndianArrayToString(data, 24, 50);
				terminal = serial_number.substring(11, 16);
				terminalCoder = serial_number.substring(14, 24);
				recordTime = getDateTime(data);*/
			} catch (Exception e)
			{
				e.printStackTrace();
			} finally
			{
				try
				{
					inputStream.close();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	public String getSerial_number()
	{
		return serial_number;
	}

	public void setSerial_number(String serial_number)
	{
		this.serial_number = serial_number;
	}

	public static void main(String[] args)
	{
		FSNInfoFile fsnInfoFile = new FSNInfoFile("D:\\test.fsn");
	}

	public String getDateTime(byte[] data)
	{
		int date = ByteHelper.UInt16ValueOfLittleEndian(data, 0);
		int time = ByteHelper.UInt16ValueOfLittleEndian(data, 2);
		int year = (date >> 9) + 1980;
		int month = date >> 5 & 0x0F;
		int day = date & 0x1F;
		int hrs = time >> 11;
		int min = time >> 5 & 0x3F;
		int sec = time << 1 & 0x3F;

		Calendar calendar = new GregorianCalendar(year, month - 1, day, hrs, min, sec);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format.format(calendar.getTime());
	}

	public String getRecordTime()
	{
		return recordTime;
	}

	public void setRecordTime(String recordTime)
	{
		this.recordTime = recordTime;
	}

	public String getTerminal()
	{
		return terminal;
	}

	public void setTerminal(String terminal)
	{
		this.terminal = terminal;
	}

	public String getTerminalCoder()
	{
		return terminalCoder;
	}

	public void setTerminalCoder(String terminalCoder)
	{
		this.terminalCoder = terminalCoder;
	}

}
