package com.fota.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import org.apache.log4j.Logger;

/*
 HeadStart由4个16比特无符号数据组成，内容为十进制数，分别是20,10,7,26;
 HeadString由6个16比特无符号数据组成，内容为
 HeadString[0] = 0
 HeadString[1] = 1
 HeadString[2] = 0x2E或0x2D
 0x2E表示该U盘数据记录包含图像序列号，0x2D表示不包含图像序列号
 HeadString[3] = 'S',大写字符S
 HeadString[4] = 'N', 大写字符N
 HeadString[5] =‘o’,小写字符o
 Counter为32比特无符号数值，记录当前冠字号码的记录数。当向冠字号码文件写入新号码记录时必须更改此数。否则，Counter与实际的记录个数不符，U盘数据将会被认为为非法文件。
 HeadEnd 由4个16比特无符号数据组成，内容为十进制数，数值分别是0,1,2,3。
 */

public class FileHeader {

	static Logger logger = Logger.getLogger(FileHeader.class);

	UInt16 headStart[]; // 4
	UInt16 headString[]; // 6
	UInt32 counter;
	UInt16 headEnd[]; // 4

	public FileHeader() {
		headStart = new UInt16[4];
		headString = new UInt16[6];
		headEnd = new UInt16[4];
	}
	
	public boolean fillData(int counter){
		headStart[0] = new UInt16(20);
		headStart[1] = new UInt16(10);
		headStart[2] = new UInt16(7);
		headStart[3] = new UInt16(26);
		headString[0] = new UInt16(0);
		headString[1] = new UInt16(1);
		headString[2] = new UInt16(0x2E);
		headString[3] = new UInt16('S');
		headString[4] = new UInt16('N');
		headString[5] = new UInt16('o');
		headEnd[0] = new UInt16(0);
		headEnd[1] = new UInt16(1);
		headEnd[2] = new UInt16(2);
		headEnd[3] = new UInt16(3);
		this.counter = new UInt32(counter);
		return true;
		
	}
	
	public boolean readFromStream(InputStream inputStream) {
		byte buf16[] = new byte[2];
		byte buf32[] = new byte[4];
		try {
			for (int i = 0; i< 4; i++) {
				inputStream.read(buf16);
				headStart[i] = UInt16.valueOfLittleEndian(buf16);
			}
			for (int i = 0; i< 6; i++) {
				inputStream.read(buf16);
				headString[i] = UInt16.valueOfLittleEndian(buf16);
			}
			inputStream.read(buf32);
			counter = UInt32.valueOfLittleEndian(buf32);
			for (int i = 0; i< 4; i++) {
				inputStream.read(buf16);
				headEnd[i] = UInt16.valueOfLittleEndian(buf16);
			}
		} catch (IOException e) {
			logger.error("Exception:",e);
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean writeToStream(OutputStream outputStream) {
		try {
			for (int i = 0; i< 4; i++) {
				outputStream.write(headStart[i].toLittleEndian());
			}
			for (int i = 0; i< 6; i++) {
				outputStream.write(headString[i].toLittleEndian());
			}
			outputStream.write(counter.toLittleEndian());
			for (int i = 0; i< 4; i++) {
				outputStream.write(headEnd[i].toLittleEndian());
			}
		} catch (IOException e) {
			logger.error("Exception:",e);
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean isValid() {
		if(headStart[0].intValue() != 20)
			return false;
		if(headStart[1].intValue() != 10)
			return false;
		if(headStart[2].intValue() != 7)
			return false;
		if(headStart[3].intValue() != 26)
			return false;
		if(headString[0].intValue() != 0)
			return false;
		if(headString[1].intValue() != 1)
			return false;
		if(headString[2].intValue() != 0x2E && headString[2].intValue() != 0x2D)
			return false;
		if(headString[3].intValue() != 'S')
			return false;
		if(headString[4].intValue() != 'N')
			return false;
		if(headString[5].intValue() != 'o')
			return false;
		if(headEnd[0].intValue() != 0)
			return false;
		if(headEnd[1].intValue() != 1)
			return false;
		if(headEnd[2].intValue() != 2)
			return false;
		if(headEnd[3].intValue() != 3)
			return false;
		return true;
	}

	public int getCounter() {
		return counter.intValue();
	}

	public void setCounter(UInt32 counter) {
		this.counter = counter;
	}

}
