package com.fota.util;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StringHelper extends org.apache.commons.lang.StringUtils
{
	public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";

	public static final String FORMAT_DATE = "yyyy-MM-dd";

	public static final String FORMAT_TIME = "HH:mm:ss";

	public static final String FORMAT_YEAR = "yyyy";

	public static final String FORMAT_YEARMONTH = "yyyy-MM";

	public static final Character YES = new Character('Y');

	public static final Character NO = new Character('N');

	/**
	 * 判断长度是否大于零
	 * 
	 * @param str
	 * @return
	 */
	public static boolean hasText(String str)
	{
		return str != null && str.length() > 0;
	}

	public static String getWINDrive()
	{
		URL location = new StringHelper().getClass().getProtectionDomain().getCodeSource().getLocation();
		if (null == location)
			return "c:";
		String replace = location.toString().replace("file:/", "");
		String der = replace.substring(0, 3);
		return der;
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static String nullToBlank(String str)
	{
		return str == null ? "" : str;
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static String nullToBlank(Object obj)
	{
		return obj == null ? "" : obj.toString();
	}

	public static boolean equalsIgnoreCase(String str, String anotherString)
	{
		return str != null && anotherString != null && str.equalsIgnoreCase(anotherString);
	}

	/**
	 * 对给定字符串增加操作符号，到指定长度
	 * 
	 * @param source
	 *            源字符串
	 * @param separator
	 *            分隔符号
	 * @param totallength
	 *            总长度
	 * @param isleft
	 *            在左边增加
	 * @return
	 */
	public static String addOperator(String source, char separator, int totallength, boolean isleft)
	{
		if (source == null || totallength <= 0)
		{
			return "";
		}
		while (source.length() < totallength)
		{
			if (isleft)
			{
				source = separator + source;
			} else
			{
				source += separator;
			}
		}
		return source;
	}

	// ***************************************************
	// 名称：dateToStr
	// 功能：将指定的日期转换成字符串
	// 输入：date: 要转换的日期;
	// fmt: 转换日期的格式, 默认为:"yyyy/MM/dd"
	// 输出：
	// 返回：转换之后的字符串
	// ***************************************************
	public static String dateToStr(java.util.Date date, String fmt)
	{
		String result = null;

		if (fmt.length() == 0)
		{
			fmt = FORMAT_DATE;
		}
		Format fmtDate = new SimpleDateFormat(fmt);
		try
		{
			result = fmtDate.format(date);
		} catch (Exception e)
		{
		}

		return result;
	}

	public static Date stringToDate(String dateString, String fmt)
	{
		if (fmt.length() == 0)
		{
			fmt = FORMAT_DATE;
		}
		SimpleDateFormat fmtDate = new SimpleDateFormat(fmt);
		Date date = null;
		try
		{
			date = fmtDate.parse(dateString);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 处理中文
	 * 
	 * @param source
	 * @return
	 */
	public static String encoding(String source)
	{
		// return source;

		if (source == null)
			return null;
		try
		{
			return new String(source.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e)
		{
			return source;
		}

	}

	public static String byteArrayToString(byte[] array, int start, int len)
	{
		String result = "";
		for (int i = start; i < len + start; i++)
		{
			if (array[i] == 0)
			{
				break;
			}
			result += (char) array[i];
		}

		return result;
	}

	public static String uint16ByteLittleEndianArrayToString(byte[] array, int len, int startPos)
	{
		String result = "";
		for (int i = 0; i < len; i++)
		{
			if (array[startPos + i * 2] == 0)
			{
				break;
			}
			result += (char) array[startPos + i * 2];
		}

		return result;
	}

	public static boolean isNullOrEmpty(String str)
	{
		return str == null || str.length() == 0;

	}

	public static String getFileExtention(String filename)
	{
		String extention = "";
		if (filename.length() > 0 && filename != null)
		{
			int i = filename.lastIndexOf(".");
			if (i > -1 && i < filename.length())
			{
				extention = filename.substring(i + 1);
			}
		}
		return extention;
	}

	public static String getFileName(String filename)
	{
		String result = "";
		if (filename.length() > 0 && filename != null)
		{
			int i = filename.lastIndexOf(".");
			if (i > -1 && i < filename.length())
			{
				result = filename.substring(i + 1);
			}
		}
		return result;
	}

	public static String limitLength(String inputString, int length, boolean patchToEnd, char patchChar)
	{
		StringBuffer stringBuffer = new StringBuffer();
		if (!patchToEnd)
		{
			for (int i = inputString.length(); i < length; i++)
			{
				stringBuffer.append(patchChar);
			}
			stringBuffer.append(inputString);
		} else
		{
			stringBuffer.append(inputString);
			for (int i = inputString.length(); i < length; i++)
			{
				stringBuffer.append(patchChar);
			}
		}
		if (inputString.length() > length)
			return stringBuffer.substring(inputString.length() - length);
		return stringBuffer.toString();
	}

	public static Timestamp string2Timestamp(String recordTime, String format)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date dt = new Date();
		try
		{
			dt = dateFormat.parse(recordTime);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String newstring = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dt);
		return Timestamp.valueOf(newstring);
	}

	public static String timestamp2String(Timestamp timestamp, String format)
	{
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(timestamp);
	}

	public static List<String> getDatesBetweenTwoDate(String d1, String d2)
	{
		List<String> dList = new ArrayList<String>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			Date dBegin = sdf.parse(d1);
			Date dEnd = sdf.parse(d2);
			dList.add(dateToStr(dEnd, "yyyyMMdd"));

			Calendar cal = Calendar.getInstance();
			cal.setTime(dEnd);
			boolean bContinue = true;
			while (bContinue)
			{

				cal.add(Calendar.DAY_OF_MONTH, -1);
				if (dBegin.before(cal.getTime()))
				{
					dList.add(dateToStr(cal.getTime(), "yyyyMMdd"));
				} else
				{
					break;
				}
			}
			dList.add(dateToStr(dBegin, "yyyyMMdd"));// 把结束时间加入集合
			return dList;
		} catch (Exception e)
		{
			// TODO: handle exception
		}
		return null;
	}
	
	public static void main(String[] args)
	{
		String addOperator = addOperator("1", '2', 7, true);
		System.out.println(addOperator);
	}
}
