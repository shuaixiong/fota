package com.fota.util;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.sun.corba.se.impl.encoding.CodeSetConversion.BTCConverter;

public class ByteHelper
{

	public static int UInt32ValueOfLittleEndian(byte[] doubleWord, int startPos)
	{
		return ((doubleWord[startPos + 3] & 0xff) << 24) | ((doubleWord[startPos + 2] & 0xff) << 16) | ((doubleWord[startPos + 1] & 0xff) << 8) | (doubleWord[startPos + 0] & 0xff);
	}

	public static int UInt16ValueOfLittleEndian(byte[] doubleWord, int startPos)
	{
		return ((doubleWord[startPos + 1] & 0xff) << 8) | (doubleWord[startPos + 0] & 0xff);
	}

	public static int Int16ValueOfLittleEndian(byte[] doubleWord, int startPos)
	{
		return ((doubleWord[startPos + 1] & 0xff) << 8) | (doubleWord[startPos + 0] & 0xff);
	}

	public static byte[] getFixedLengthByteArray(int len, String data)
	{
		if (!StringHelper.isNullOrEmpty(data))
		{
			byte[] bytes = data.getBytes();
			if (len < bytes.length)
				return null;
			byte[] dataTemp = new byte[len - bytes.length];
			for (int i = 0; i < dataTemp.length; i++)
			{
				dataTemp[i] = "_".getBytes()[0];
			}
			byte[] allData = byteMerger(dataTemp, bytes);
			return allData;
		} else
		{
			byte[] dataTemp = new byte[len];
			for (int i = 0; i < dataTemp.length; i++)
			{
				dataTemp[i] = "_".getBytes()[0];
			}
			return dataTemp;
		}
	}

	// java 合并两个byte数组
	public static byte[] byteMerger(byte[] byte_1, byte[] byte_2)
	{
		byte[] byte_3 = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
		System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
		return byte_3;
	}

	public static byte[] getDateGM(String data)
	{
		// 20150626090102
		int year = Integer.valueOf(data.substring(0, 4));
		int Month = Integer.valueOf(data.substring(4, 6));
		int day = Integer.valueOf(data.substring(6, 8));
		int i = year - 1980;
		year = i << 9;
		Month = Month << 5;
		int date1 = year + Month + day;
		byte[] int2byte = int2byte(date1);
		return int2byte;
	}

	public static byte[] getTimeGM(String data)
	{
		// 20150626090102
		// time = (Hour<<11) + (Minute<<5) + (Second>>1)
		int HH = Integer.valueOf(data.substring(8, 10));
		int MM = Integer.valueOf(data.substring(10, 12));
		int SS = Integer.valueOf(data.substring(12, 14));
		HH = HH << 11;
		MM = MM << 5;
		SS = SS >> 1;
		int tiem = HH + MM + SS;
		byte[] int2byte = int2byte(tiem);
		return int2byte;
	}

	public static void main(String[] args)
	{
		byte[] dateGM = getDateGM("20150626090102");
		int byte2Int = byte2Int(dateGM);
		byte[] byteArray = toByteArray("C0000000000000", 6);
		System.out.println();
	}

	/** 32位int转byte[] */
	public static byte[] int4byte(int res)
	{
		byte[] targets = new byte[4];
		targets[0] = (byte) (res & 0xff);// 最低位
		targets[1] = (byte) ((res >> 8) & 0xff);// 次低位
		targets[2] = (byte) ((res >> 16) & 0xff);// 次高位
		targets[3] = (byte) (res >>> 24);// 最高位,无符号右移。
		return targets;
	}

	public static byte[] int2Bytes(int num)
	{
		byte[] byteNum = new byte[4];
		for (int ix = 0; ix < 4; ++ix)
		{
			int offset = 32 - (ix + 1) * 8;
			byteNum[ix] = (byte) ((num << offset) & 0xff);
		}
		return byteNum;
	}

	/** 2 byte[] 16位int */
	public static int byte2Int(byte[] time)
	{
		return ((time[1] & 0xff) << 8) | (time[0] & 0xff);
	}

	/** 16位int转byte[] */
	public static byte[] int2byte(int res)
	{
		byte[] targets = new byte[2];
		targets[0] = (byte) (res & 0xff);// 最低位
		targets[1] = (byte) ((res >> 8) & 0xff);// 次低位
		return targets;
	}

	public static byte[] toByteArray(String hexString, int len)
	{
		if (StringUtils.isEmpty(hexString))
			throw new IllegalArgumentException("this hexString must not be empty");

		hexString = hexString.toLowerCase();
		final byte[] byteArray = new byte[len];
		int k = 0;
		for (int i = 0; i < byteArray.length; i++)
		{// 因为是16进制，最多只会占用4位，转换成字节需要两个16进制的字符，高位在先
			byte high = (byte) (Character.digit(hexString.charAt(k), 16) & 0xff);
			byte low = (byte) (Character.digit(hexString.charAt(k + 1), 16) & 0xff);
			byteArray[i] = (byte) (high << 4 | low);
			k += 2;
		}
		return byteArray;
	}

	public static String toHexString(byte[] byteArray)
	{
		if (byteArray == null || byteArray.length < 1)
			throw new IllegalArgumentException("this byteArray must not be null or empty");

		final StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < byteArray.length; i++)
		{
			if ((byteArray[i] & 0xff) < 0x10)// 0~F前面不零
				hexString.append("0");
			hexString.append(Integer.toHexString(0xFF & byteArray[i]));
		}
		return hexString.toString().toLowerCase();
	}

	public static byte[] longToByte(long number)
	{
		long temp = number;
		byte[] b = new byte[6];
		for (int i = 0; i < b.length; i++)
		{
			b[i] = new Long(temp & 0xff).byteValue();// 将最低位保存在最低位
			temp = temp >> 8; // 向右移8位
		}
		return b;
	}
}
