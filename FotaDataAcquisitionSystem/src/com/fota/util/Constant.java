package com.fota.util;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Table;

public class Constant
{
	public static String configPath=StringHelper.getWINDrive()+"/CollectionData/config/fotadata.properties";
	public static String configLog=StringHelper.getWINDrive()+"/CollectionData/config/log4j.properties";
	public static int showMessageCount=100;
	public static int serialDatalen=6;
	public static String rootFilePath=StringHelper.getWINDrive()+"/CollectionData";
	public static String uploadFilePath=StringHelper.getWINDrive()+"/CollectionData/Datas";
	
	
	
	public static List listView;
	public static Table table;
	public static Label countLabel;
	public static Label doubtLabel;
	public static int doubtCout;
	
	public static List getListView()
	{
		return listView;
	}

	public static void setListView(List lisView)
	{
		Constant.listView = lisView;
	}

	public static Table getTable()
	{
		return table;
	}

	public static void setTable(Table table)
	{
		Constant.table = table;
	}

	public static Label getCountLabel()
	{
		return countLabel;
	}

	public static void setCountLabel(Label countLabel)
	{
		Constant.countLabel = countLabel;
	}

	public static Label getDoubtLabel()
	{
		return doubtLabel;
	}

	public static void setDoubtLabel(Label doubtLabel)
	{
		Constant.doubtLabel = doubtLabel;
	}
}
