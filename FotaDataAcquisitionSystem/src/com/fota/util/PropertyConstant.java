package com.fota.util;

public class PropertyConstant
{
	public static String LOCALHOSTPORT = "localhostPort";
	public static String SERVERIP = "ServerIp";
	public static String SERVERPORT = "serverPort";
	public static String FSNFILEPATH = "FSNFilePath";
	public static String USERNUMBER = "userNumber";
	/**
	 * 报送银行编号
	 */
	public static String SUBMITTEDBANKCODE = "submittedBankCode";
	/**
	 * 是否清分中心
	 */
	public static String ISCLEARCENTER = "isClearCenter";

	public static String BUNDLENUMBER = "bundleNumber";
	public static String PKGNUMBER = "pkgNumber";
	public static String MAXNUMBER = "maxnum";

}
