package com.fota.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author FOTA
 * 
 */
public class TimeUtil
{

	public final static String FORMAT_YEAR = "yyyy";

	public final static String FORMAT_MONTH_DAY = "MM月dd日";

	public final static String FORMAT_DATE = "yyyy-MM-dd";

	public final static String FORMAT_DATE_1 = "yyyyMMdd";

	public final static String FORMAT_TIME = "HH:mm";

	public final static String FORMAT_MONTH_DAY_TIME = "MM月dd日  hh:mm";

	public final static String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm";

	public final static String FORMAT_DATE_TIME1 = "yyyy-MM-dd HH:mm:ss";

	public final static String FORMAT_DATE_TIME2 = "yyyyMMddHHmmss";

	public final static String FORMAT_DATE1_TIME = "yyyy/MM/dd HH:mm";

	public final static String FORMAT_DATE_TIME_SECOND = "yyyy/MM/dd HH:mm:ss";

	public static final int SHOW_TYPE_SIMPLE = 0;

	public static final int SHOW_TYPE_COMPLEX = 1;

	public static final int SHOW_TYPE_ALL = 2;

	private static final long ONEDAY = 86400000;

	public static final int SHOW_TYPE_CALL_LOG = 3;

	public static final int SHOW_TYPE_CALL_DETAIL = 4;

	private static SimpleDateFormat sdf = new SimpleDateFormat();

	private static final int YEAR = 365 * 24 * 60 * 60;// 年

	private static final int MONTH = 30 * 24 * 60 * 60;// 月

	private static final int DAY = 24 * 60 * 60;// 天

	private static final int HOUR = 60 * 60;// 小时

	private static final int MINUTE = 60;// 分钟

	public static final TimeZone tz = TimeZone.getTimeZone("GMT+8:00");

	public static final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 根据时间戳获取描述性时间，如3分钟前，1天前
	 * 
	 * @param timestamp
	 *            时间戳 单位为毫秒
	 * @return 时间字符串
	 */
	public static String getDescriptionTimeFromTimestamp(long timestamp)
	{
		long currentTime = System.currentTimeMillis();
		long timeGap = (currentTime - timestamp) / 1000;// 与现在时间相差秒数
		System.out.println("timeGap: " + timeGap);
		String timeStr = null;
		if (timeGap > YEAR)
		{
			timeStr = timeGap / YEAR + "年前";
		} else if (timeGap > MONTH)
		{
			timeStr = timeGap / MONTH + "个月前";
		} else if (timeGap > DAY)
		{// 1天以上
			timeStr = timeGap / DAY + "天前";
		} else if (timeGap > HOUR)
		{// 1小时-24小时
			timeStr = timeGap / HOUR + "小时前";
		} else if (timeGap > MINUTE)
		{// 1分钟-59分钟
			timeStr = timeGap / MINUTE + "分钟前";
		} else
		{// 1秒钟-59秒钟
			timeStr = "刚刚";
		}
		return timeStr;
	}

	/**
	 * 获取当前日期的指定格式的字符串
	 * 
	 * @param format
	 *            指定的日期时间格式，若为null或""则使用指定的格式"yyyy-MM-dd HH:MM"
	 * @return
	 */
	public static String getCurrentTime(String format)
	{
		if (format == null || format.trim().equals(""))
		{
			sdf.applyPattern(FORMAT_DATE_TIME);
		} else
		{
			sdf.applyPattern(format);
		}
		return sdf.format(new Date());
	}

	/**
	 * @param data
	 *            类型转换为String类型
	 * @param formatType
	 *            格式为yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
	 * @return
	 */
	public static String dateToString(Date data, String formatType)
	{
		return new SimpleDateFormat(formatType).format(data);
	}

	/**
	 * long类型转换为String类型
	 * 
	 * @param currentTime
	 *            要转换的long类型的时间
	 * @param formatType
	 *            要转换的string类型的时间格式
	 * @return
	 */
	public static String longToString(long currentTime, String formatType)
	{
		String strTime = "";
		Date date = longToDate(currentTime, formatType);// long类型转成Date类型
		strTime = dateToString(date, formatType); // date类型转成String
		return strTime;
	}

	/**
	 * string类型转换为date类型
	 * 
	 * @param strTime
	 *            要转换的string类型的时间，formatType要转换的格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日
	 * @param formatType
	 *            strTime的时间格式必须要与formatType的时间格式相同
	 * @return
	 */
	public static Date stringToDate(String strTime, String formatType)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(formatType);
		Date date = null;
		try
		{
			date = formatter.parse(strTime);
		} catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * long转换为Date类型
	 * 
	 * @param currentTime
	 *            要转换的long类型的时间
	 * @param formatType
	 *            要转换的时间格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日 HH时mm分ss秒
	 * @return
	 */
	public static Date longToDate(long currentTime, String formatType)
	{
		Date dateOld = new Date(currentTime); // 根据long类型的毫秒数生命一个date类型的时间
		String sDateTime = dateToString(dateOld, formatType); // 把date类型的时间转换为string
		Date date = stringToDate(sDateTime, formatType); // 把String类型转换为Date类型
		return date;
	}

	/**
	 * string类型转换为long类型
	 * 
	 * @param strTime
	 *            要转换的String类型的时间 strTime的时间格式和formatType的时间格式必须相同
	 * @param formatType
	 *            时间格式
	 * @return
	 */
	public static long stringToLong(String strTime, String formatType)
	{
		Date date = stringToDate(strTime, formatType); // String类型转成date类型
		if (date == null)
		{
			return 0;
		} else
		{
			long currentTime = dateToLong(date); // date类型转成long类型
			return currentTime;
		}
	}

	/**
	 * date类型转换为long类型
	 * 
	 * @param date
	 *            要转换的date类型的时间
	 * @return
	 */
	public static long dateToLong(Date date)
	{
		return date.getTime();
	}

	public static String getTime(long time)
	{
		SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm");
		return format.format(new Date(time));
	}

	public static String getHourAndMin(long time)
	{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(new Date(time));
	}

	public static String getDateString(long time, int type)
	{
		Calendar c = Calendar.getInstance();
		c = Calendar.getInstance(tz);
		c.setTimeInMillis(time);
		long currentTime = System.currentTimeMillis();
		Calendar current_c = Calendar.getInstance();
		current_c = Calendar.getInstance(tz);
		current_c.setTimeInMillis(currentTime);

		int currentYear = current_c.get(Calendar.YEAR);
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH) + 1;
		int d = c.get(Calendar.DAY_OF_MONTH);
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		int second = c.get(Calendar.SECOND);
		long t = currentTime - time;
		long t2 = currentTime - getCurrentDayTime();
		String dateStr = "";
		if (t < t2 && t > 0)
		{
			if (type == SHOW_TYPE_SIMPLE)
			{
				dateStr = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else if (type == SHOW_TYPE_COMPLEX)
			{
				dateStr = "今天  " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else if (type == SHOW_TYPE_CALL_LOG)
			{
				dateStr = "今天  " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else if (type == SHOW_TYPE_CALL_DETAIL)
			{
				dateStr = "今天  ";
			} else
			{
				dateStr = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
			}
		} else if (t < (t2 + ONEDAY) && t > 0)
		{
			if (type == SHOW_TYPE_SIMPLE || type == SHOW_TYPE_CALL_DETAIL)
			{
				dateStr = "昨天  ";
			} else if (type == SHOW_TYPE_COMPLEX)
			{
				dateStr = "昨天  " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else if (type == SHOW_TYPE_CALL_LOG)
			{
				dateStr = "昨天  " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else
			{
				dateStr = "昨天  " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":" + (second < 10 ? "0" + second : second);
			}
		} else if (y == currentYear)
		{
			if (type == SHOW_TYPE_SIMPLE)
			{
				dateStr = (m < 10 ? "0" + m : m) + "/" + (d < 10 ? "0" + d : d);
			} else if (type == SHOW_TYPE_COMPLEX)
			{
				dateStr = (m < 10 ? "0" + m : m) + "月" + (d < 10 ? "0" + d : d) + "日";
			} else if (type == SHOW_TYPE_CALL_LOG || type == SHOW_TYPE_COMPLEX)
			{
				dateStr = (m < 10 ? "0" + m : m) + /* 月 */"/" + (d < 10 ? "0" + d : d) + /* 日 */" " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
			} else if (type == SHOW_TYPE_CALL_DETAIL)
			{
				dateStr = y + "/" + (m < 10 ? "0" + m : m) + "/" + (d < 10 ? "0" + d : d);
			} else
			{
				dateStr = (m < 10 ? "0" + m : m) + "月" + (d < 10 ? "0" + d : d) + "日 " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":"
						+ (second < 10 ? "0" + second : second);
			}
		} else
		{
			if (type == SHOW_TYPE_SIMPLE)
			{
				dateStr = y + "/" + (m < 10 ? "0" + m : m) + "/" + (d < 10 ? "0" + d : d);
			} else if (type == SHOW_TYPE_COMPLEX)
			{
				dateStr = y + "年" + (m < 10 ? "0" + m : m) + "月" + (d < 10 ? "0" + d : d) + "日";
			} else if (type == SHOW_TYPE_CALL_LOG || type == SHOW_TYPE_COMPLEX)
			{
				dateStr = y + /* 年 */"/" + (m < 10 ? "0" + m : m) + /* 月 */"/" + (d < 10 ? "0" + d : d) + /* 日 */"  "/*
																												 * + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute)
																												 */;
			} else if (type == SHOW_TYPE_CALL_DETAIL)
			{
				dateStr = y + "/" + (m < 10 ? "0" + m : m) + "/" + (d < 10 ? "0" + d : d);
			} else
			{
				dateStr = y + "年" + (m < 10 ? "0" + m : m) + "月" + (d < 10 ? "0" + d : d) + "日 " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute) + ":"
						+ (second < 10 ? "0" + second : second);
			}
		}
		return dateStr;
	}

	public static long getCurrentTime()
	{
		return System.currentTimeMillis() / 1000;
	}

	/**
	 * 获取当前当天日期的毫秒数 2012-03-21的毫秒数
	 * 
	 * @return
	 */
	public static long getCurrentDayTime()
	{
		Date d = new Date(System.currentTimeMillis());
		String formatDate = yearFormat.format(d);
		try
		{
			return (yearFormat.parse(formatDate)).getTime();
		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public static int compareDate(String DATE1, String DATE2)
	{

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime())
			{
				System.out.println("dt1 在dt2前");
				return 1;
			} else if (dt1.getTime() < dt2.getTime())
			{
				System.out.println("dt1在dt2后");
				return -1;
			} else
			{
				return 0;
			}
		} catch (Exception exception)
		{
			exception.printStackTrace();
		}
		return 0;
	}

	public static void main(String[] args)
	{
		compareDate(dateToString(new Date(), FORMAT_DATE), "2016-07-15");
	}
}