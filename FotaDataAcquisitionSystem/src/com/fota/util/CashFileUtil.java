package com.fota.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.swt.events.MenuDetectEvent;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class CashFileUtil
{
	// private static final String CASHFILE_FOLDER_NORMAL = "Datas";
	private static final String CASHFILE_FOLDER_NORMAL = "/";
	private static final String CASHFILE_FOLDER_EXCEPTION = "exception";
	private static final String CASHFILE_FOLDER_BACKUP = "fsnbackup";
	private static final String CASHFILE_FOLDER_EXPORT = "fsnexport";
	private static final String TRANSACTION_FILE_FOLDER_EXPORT = "transactionexport";
	private static final String TRANSACTION_IMAGE_EXT = ".tsi";
	private static final String TRANSACTION_FILE_EXT = ".tsf";
	private static final String TRANSACTION_ZIPFILE_EXT = ".tsz";
	private static final String PIC_FOLDER_IN_WEB = "pic";
	static final String FOTA_PREFIX = "10";
	static Logger logger = Logger.getLogger(CashFileUtil.class);
	private static DataSource dataSource;
	// private static IdWorker idWorker = new IdWorker(0, 0);
	private static ExecutorService fsnFileParserThreadPool = Executors.newFixedThreadPool(2);
	private static ExecutorService transactionFileParserThreadPool = Executors.newFixedThreadPool(2);
	private static ExecutorService cashFileParserThreadPool = Executors.newCachedThreadPool();
	// private static ExecutorService execBatchThreadPool = Executors
	// .newCachedThreadPool();
	private static ExecutorService copyFSNThreadPool = Executors.newSingleThreadExecutor();
	private static ExecutorService execBatchThreadPool = Executors.newSingleThreadExecutor();
	private static ExecutorService backupCashFileThreadPool = Executors.newSingleThreadExecutor();
	private static ExecutorService saveImageThreadPool = Executors.newSingleThreadExecutor();
	private static String cashManagerPath;
	private static boolean saveImage = true;

	public static class ImgInfo
	{
		private String filename;
		private String path;
		private String relativePath;
		private String filetype;

		public String getFilename()
		{
			return filename;
		}

		public void setFilename(String filename)
		{
			this.filename = filename;
		}

		public String getPath()
		{
			return path;
		}

		public void setPath(String path)
		{
			this.path = path;
		}

		public String getFiletype()
		{
			return filetype;
		}

		public void setFiletype(String filetype)
		{
			this.filetype = filetype;
		}

		public String getRelativePath()
		{
			return relativePath;
		}

		public void setRelativePath(String relativePath)
		{
			this.relativePath = relativePath;
		}

	}

	public static DataSource getDataSource()
	{
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource)
	{
		CashFileUtil.dataSource = dataSource;
	}

	public static void setImageSave(boolean saveImage)
	{
		CashFileUtil.saveImage = saveImage;
	}

	// public static boolean parseFSNFileWithBusiInfo(String cashFilename, String busiInfoFilename){
	// BusiInfoFile busiInfoFile = null;
	// if (busiInfoFilename != null) {
	// busiInfoFile = readBusiInfoFile(busiInfoFilename);
	// if (busiInfoFile == null || !busiInfoFile.isValid())
	// return false;
	// }
	// FileInputStream inputStream = null;
	// try {
	// inputStream = new FileInputStream(cashFilename);
	// FSNCache fsnFile = new FSNCache(inputStream);
	// return FSN2PreparedStamente(busiInfoFile, fsnFile, "", "", "", "");
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// logger.error("Exception:", e);
	// } finally {
	// if (inputStream != null) {
	// try {
	// inputStream.close();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// }
	// return false;
	// }

	protected static boolean checkDuplicateTransaction(Long transactionId)
	{
		if (dataSource == null)
			return false;
		Connection conn = null;
		Statement st = null;
		conn = DataSourceUtils.getConnection(dataSource);
		try
		{

			String sql = "SELECT * FROM cashmgr.transaction_log " + "where  id = " + transactionId;
			logger.info("exec sql:" + sql);
			st = conn.createStatement();
			ResultSet set = st.executeQuery(sql);
			while (set.next())
			{
				logger.info("duplicated transation. transaction id:" + transactionId);
				return true;
			}
			return false;
		} catch (SQLException e)
		{
			e.printStackTrace();
			logger.error("Exception:", e);

		} finally
		{
			try
			{
				if (st != null)
					st.close();
			} catch (SQLException se2)
			{
			}
			try
			{
				if (conn != null)
					conn.close();
			} catch (SQLException se)
			{
				se.printStackTrace();
			}
		}
		return false;
	}

	public static String getTransactionImageFilename(String recordTime, long transactionId)
	{
		String path = getFullPathInWeb(PIC_FOLDER_IN_WEB);
		if (StringUtils.isEmpty(recordTime))
		{
			path += "/" + "empty";
		} else if (recordTime.length() < 12)
		{
			path += "/" + recordTime;
		} else
		{
			path += "/" + recordTime.substring(0, 8);
			path += "/" + recordTime.substring(8, 12);
		}
		path += "/" + String.valueOf(transactionId) + ".fsn";
		return path;
	}

	public static String convertWildcard(String bankword)
	{
		// TODO maybe not need
		// for (int i = 0; i < bankword.length(); i++) {
		// if (!isValidBankwordChar(bankword.charAt(i))) {
		// logger.info("bankword have wildcard:" + bankword);
		// bankword = bankword.replace(bankword.charAt(i),
		// BANKWORD_WILDCARD_CHAR);
		// }
		// }
		return bankword;
	}

	private static boolean isValidBankwordChar(char ch)
	{
		if ((ch <= 'Z' && ch >= 'A') || (ch <= '9' && ch >= '0'))
		{
			return true;
		}
		return false;
	}

	public static void setCashManagerPath(String cashManagerPath)
	{
		logger.info("CashManagerPath:" + cashManagerPath);
		CashFileUtil.cashManagerPath = cashManagerPath;
	}

	public static String getCashManagerPath()
	{
		return CashFileUtil.cashManagerPath;
	}

	public static String getFullPathInWeb(String filename)
	{
		return CashFileUtil.cashManagerPath + "/" + filename;
	}

	private static List<String> fPaths = new ArrayList<>();
	private static String prevFile = null;
	private static boolean is = false;

	public static boolean saveCashFileToFolder(String filename, InputStream inputStream, boolean append)
	{
		// 接收FSN,txt文件
		OutputStream outputStream = null;
		
		try
		{
			File file = new File(filename);
			if (append)
				file.createNewFile();
			outputStream = new FileOutputStream(file, append);
			int read = 0;
			byte[] bytes = new byte[1024 * 10];

			while ((read = inputStream.read(bytes)) != -1)
			{
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			inputStream.close();
			outputStream.close();
			String ext = StringHelper.getFileExtention(filename);
			if (ext.equalsIgnoreCase("fsn"))
			{
				FotaUtil.show("收到FSN文件！");
				if (null != prevFile)
				{
					is = MD5Utils.compareFileMid(prevFile, file.getAbsolutePath());
					if (is)
					{
						FotaUtil.show("收到相同的FSN文件,已去重！");
						file.delete();
					}
				}
				if (!is)
				{
					prevFile = file.getAbsolutePath();
				}
			} else if (ext.equals("txt"))
			{
				if (is)
				{
					file.delete();
				}
			}

			if (ext.equalsIgnoreCase("fsn") && !is)
			{
				fPaths.add(filename);
			} else if (ext.equalsIgnoreCase("txt") && !is)
			{
				fPaths.add(0, filename);
				// CRHUtils.addCRHFile(filename);
			}
			if (fPaths.size() == 2)
			{
				List<String> temp = (ArrayList<String>) fPaths;
				fPaths = new ArrayList<>();
				GZHUtils.mergerFSN(temp);
			}

			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
			logger.error("Exception:", e);

		} finally
		{
		}
		return false;
	}

	// static Integer folderindex = 0;

	public static String createCashFileFolder(String date, String terminalCode)
	{
		// String fullFolderName = generateCashFileFolderPath(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/" + CashFileUtil.CASHFILE_FOLDER_NORMAL, date,
		// terminalCode);
		String fullFolderName = PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/";
		// synchronized (folderindex) {
		// folderindex++;
		// }
		// fullFolderName += "_"+folderindex;
		File folder = new File(fullFolderName);
		if (!folder.exists())
		{
			folder.mkdirs();
			logger.info("created folder:" + fullFolderName);
		}
		return fullFolderName;
	}

	public static String createCashFileBackupFolder(String date, String terminalCode, boolean isUpload)
	{
		String fullFolderName = generateCashFileBackupFolderPath(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/" + CashFileUtil.CASHFILE_FOLDER_BACKUP, date,
				terminalCode, isUpload);
		// synchronized (folderindex) {
		// folderindex++;
		// }
		// fullFolderName += "_"+folderindex;
		File folder = new File(fullFolderName);
		if (!folder.exists())
		{
			folder.mkdirs();
			logger.info("created folder:" + fullFolderName);
		}
		return fullFolderName;
	}

	/*
	 * 1 每天每台机器打包一个FSN和一个TXT文件；
	 * 
	 * 命名规则为:时间（年月日）_地区号_支行号_网点号_机器编号.txt 时间（年月日）_地区号_支行号_网点号_机器编号.fsn
	 * 
	 * 2 文件夹可以指定路径（这个需求如果不方便修改，暂时可以类似我们之前的FSNBackup自己定义一个固定的位置存放）
	 * 
	 * 3 文件夹层次设置: 每日数据打包-》时间（年月日）-》地区号-》支行号-》网点号（所有网点机器的每日打包文件都放这里）
	 */
	public static String createCashFileExportFolder(String date, String districtCode, String branchCode, String subBranchCode)
	{
		String fullFolderName = PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/" + CashFileUtil.CASHFILE_FOLDER_EXPORT + "/" + date + "/" + districtCode + "/"
				+ branchCode + "/" + subBranchCode;
		File folder = new File(fullFolderName);
		if (!folder.exists())
		{
			folder.mkdirs();
			logger.info("created folder:" + fullFolderName);
		}
		return fullFolderName;
	}

	public static String createTransactionFileExportFolder(String date, String districtCode, String branchCode, String subBranchCode, String terminalCode)
	{
		String fullFolderName = PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/" + CashFileUtil.TRANSACTION_FILE_FOLDER_EXPORT + "/" + date + "/" + districtCode + "/"
				+ branchCode + "/" + subBranchCode + "/" + terminalCode;
		File folder = new File(fullFolderName);
		if (!folder.exists())
		{
			folder.mkdirs();
			logger.info("created folder:" + fullFolderName);
		}
		return fullFolderName;
	}

	public static String getCashFileExportFolderName()
	{
		return PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/" + CashFileUtil.CASHFILE_FOLDER_EXPORT;

	}

	/*
	 * 4. 采集服务接收到机具编号，依格式截取地区号、网点号生成目录，目录地址格式为：日期(yyyymmdd)/地区号/网点号/机器编号/ t_yyyymmddHHMMSSsss， 还需要再生成一个唯一的子目录，此子目录用于存放本次冠字号上传的文件。该子目录生成的规则为。 FSN文件保留：在合适的位置建立FSN_Backup文件夹保留上传的FSN文件。
	 * 路径为：……/FSN_Backup/日期（年月日）/地区号/网点号/机具编号/
	 * 
	 * FSN文件命名规则： FSN24位机具编号中公司缩写_日期（年月日时分秒）_地区号_网点号_FSN24位机具编号后13位中的所有有效字符.FSN (若没有地区号网点号信息可以用字符“0”代替)
	 * 
	 * FSN文件命名实例：FOTA_20141120142512_1234567_8901210_00011100002.FSN
	 */
	public static String generateCashFileFolderPath(String base, String date, String ternimalCode)
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

		return base + "/" + date + "/" + ternimalCode.substring(0, 4) + "/" + ternimalCode.substring(8, 12) + "/" + ternimalCode.substring(14, 24) + "/t_" + simpleDateFormat.format(new Date());
	}

	// fsnbackup/日期(yyyymmdd)/地区号/网点号/机器编号/yyyymmddHHMMSSsss
	public static String generateCashFileBackupFolderPath(String base, String date, String ternimalCode, boolean isUpload)
	{

		if (isUpload)
		{
			return base + "/" + date.substring(0, 8) + "/" + ternimalCode + "/" + date;
		} else
		{
			return base + "/" + date.substring(0, 8) + "/" + ternimalCode.substring(0, 4) + "/" + ternimalCode.substring(8, 12) + "/" + ternimalCode.substring(14) + "/" + date;
		}

	}

}
