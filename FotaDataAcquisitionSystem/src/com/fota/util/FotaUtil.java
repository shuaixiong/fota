package com.fota.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.channels.FileChannel;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class FotaUtil
{
	private static CountDownLatch countDownLatch;
	private Logger logger = Logger.getLogger(this.getClass());

	public static void getLocalIP1() throws Exception
	{
		Enumeration e1 = (Enumeration) NetworkInterface.getNetworkInterfaces();
		while (e1.hasMoreElements())
		{
			NetworkInterface ni = (NetworkInterface) e1.nextElement();
			System.out.print(ni.getName());
			System.out.print(": ");
			Enumeration e2 = ni.getInetAddresses();
			while (e2.hasMoreElements())
			{
				InetAddress ia = (InetAddress) e2.nextElement();
				if (ia instanceof Inet6Address)
					continue; // omit IPv6 address
				System.out.print(ia.getHostAddress());
				if (e2.hasMoreElements())
				{
					System.out.print(", ");
				}
			}
			System.out.print("\n");
		}
	}

	public static void setCountDownLatch(int size)
	{
		countDownLatch = new CountDownLatch(size);
	}

	public static void countDown()
	{
		countDownLatch.countDown();
	}

	public static CountDownLatch getCountDownLatch()
	{
		return countDownLatch;
	}

	public static void setCountDownLatch(CountDownLatch countDownLatch)
	{
		FotaUtil.countDownLatch = countDownLatch;
	}

	public static String getLocalIP()
	{
		Enumeration e1 = null;
		try
		{
			e1 = (Enumeration) NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e)
		{
			return "127.0.0.1";
		}
		while (e1.hasMoreElements())
		{
			NetworkInterface ni = (NetworkInterface) e1.nextElement();
			// System.out.print(ni.getName());
			// System.out.print(": ");
			Enumeration e2 = ni.getInetAddresses();
			while (e2.hasMoreElements())
			{
				InetAddress ia = (InetAddress) e2.nextElement();
				if (ia instanceof Inet6Address)
					continue; // omit IPv6 address
				String hostAddress = ia.getHostAddress();

				if (!"".equals(hostAddress) && !"127.0.0.1".equals(hostAddress))
				{
					return hostAddress;
				}
				if (e2.hasMoreElements())
				{
				}
			}

		}
		return "127.0.0.1";
	}

	/**
	 * @param file
	 * @param resultFileName
	 * @return
	 */
	public static List<File> ergodic(String filepath, List<File> resultFileName)
	{
		File file = new File(filepath);
		File[] files = file.listFiles();
		if (files == null)
			return resultFileName;// 判断目录下是不是空的
		for (File f : files)
		{
			if (f.isDirectory())
			{// 判断是否文件夹
				// resultFileName.add(f.getPath());
				ergodic(f.getAbsolutePath(), resultFileName);// 调用自身,查找子目录
			} else
				resultFileName.add(f);
		}
		return resultFileName;
	}

	public static String getExtName(String s, char split)
	{
		int i = s.indexOf(split);
		int leg = s.length();
		return (i > 0 ? (i + 1) == leg ? " " : s.substring(i, s.length()) : " ");
	}

	public static File renameFile(String path, String oldname, String newname)
	{
		newname = newname + getExtName(oldname, '.');
		if (!oldname.equals(newname))
		{// 新的文件名和以前文件名不同时,才有必要进行重命名
			File oldfile = new File(path + "/" + oldname);
			File newfile = new File(path + "/" + newname);
			if (!oldfile.exists())
			{
				return oldfile;// 重命名文件不存在
			}
			if (newfile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
				System.out.println(newname + "已经存在！");
			else
			{
				oldfile.renameTo(newfile);
				return newfile;
			}
		} else
		{
			System.out.println("新文件名和旧文件名相同...");
			return new File(path + "/" + oldname);
		}
		return null;
	}

	public static void cleanOldUploadFiles(String filepath)
	{
		File file = new File(filepath);
		File[] files = file.listFiles();
		if (null != files)
		{
			try
			{
				for (int i = 0; i < files.length; i++)
				{
					FileUtils.deleteDirectory(files[i]);
				}

			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void cleanOldUploadFile(String filepath)
	{
		File file = new File(filepath);
		if (file.exists())
		{
			try
			{
				FileUtils.deleteDirectory(file.getParentFile());
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void initCleanFile(String filepath)
	{
		File file = new File(filepath);
		File[] listFiles = file.listFiles();
		if (null != listFiles)
		{
			for (File file2 : listFiles)
			{
				try
				{
					FileUtils.deleteDirectory(file2);
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args)
	{
		// List<File> resultFileName = new ArrayList<>();
		// List<File> renameFiles = new ArrayList<>();
		// List<File> ergodic = ergodic(PropertyReader.getValue(Constant.configPath, PropertyConstant.FSNFILEPATH) + "/upload/", resultFileName);
		// for (File file : ergodic)
		// {
		// File renameFile = renameFile(file.getParent(), file.getName(), "1234567890");
		// renameFiles.add(renameFile);
		// }
		initFoder();
		System.out.println();
	}

	public static void show(final String message)
	{
		Display.getDefault().syncExec(new Runnable()
		{
			public void run()
			{
				if (Constant.listView.getItems().length >= Constant.showMessageCount)
				{
					Constant.listView.removeAll();
				}
				String time = TimeUtil.getCurrentTime(TimeUtil.FORMAT_DATE_TIME1);
				Constant.listView.add(time + "     " + message);
				Constant.listView.setSelection(Constant.listView.getItems().length - 1);
			}
		});
	}

	public static void showBanknoteWord(final BusiInfoFile busiInfoFile)
	{
		Display.getDefault().syncExec(new Runnable()
		{
			public void run()
			{
				Table table = Constant.table;
				if (null != table)
				{
					TableItem item = new TableItem(table, SWT.NONE);
					long Ltime = TimeUtil.stringToLong(busiInfoFile.getRecordTime(), TimeUtil.FORMAT_DATE_TIME2);
					String time = TimeUtil.longToString(Ltime, TimeUtil.FORMAT_DATE_TIME_SECOND);
					String banknoteWord = busiInfoFile.getBanknoteWordNumber();
					String version = busiInfoFile.getVersion();
					String curValue = busiInfoFile.getCurValue();
					String curType = busiInfoFile.getCurType();
					String ser = busiInfoFile.getSerialNumber();
					String doubt = busiInfoFile.getStats().equals("0") ? "可疑币" : "真币";

					// String doubt = "假";
					item.setText(new String[] { time, banknoteWord, version, curType, curValue, doubt, ser });
					int count = table.getItemCount();
					Constant.countLabel.setText("点钞张数：" + count + "张");
					if (doubt.equals("可疑币"))
					{
						Constant.doubtCout++;
						Constant.doubtLabel.setText("可疑币张数：" + Constant.doubtCout + "张");
						item.setBackground(table.getParent().getDisplay().getSystemColor(SWT.COLOR_DARK_RED));
						item.setForeground(table.getParent().getDisplay().getSystemColor(SWT.COLOR_GRAY));
					}

				} else
				{
					Constant.doubtCout = 0;
				}
			}
		});
	}

	public static byte[] byteMerger(byte[] byte_1, byte[] byte_2)
	{
		byte[] byte_3 = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
		System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
		return byte_3;
	}

	public static void initFoder()
	{
		String rootPathname = Constant.rootFilePath;
		File rootFile = new File(rootPathname);
		try
		{
			if (!rootFile.exists())
			{
				rootFile.mkdirs();
				rootFile = new File(rootFile.getAbsolutePath() + "/config/");
				rootFile.mkdirs();
				rootFile = new File(rootFile.getParent() + "/Datas/");
				rootFile.mkdirs();
				rootFile = new File(rootFile.getParent() + "/CRH/");
				rootFile.mkdirs();
				rootFile = new File(rootFile.getParent() + "/CRHTEMP/");
				rootFile.mkdirs();
				File rootFile1 = new File(rootFile.getParent() + "/CRHTEMP/1");
				rootFile1.mkdirs();
				File rootFile2 = new File(rootFile.getParent() + "/CRHTEMP/2");
				rootFile2.mkdirs();
				File rootFile6 = new File(rootFile.getParent() + "/CRHTEMP/CRH");
				rootFile6.mkdirs();
				File rootFile9 = new File(rootFile.getParent() + "/CRHTEMP/ResidualData");
				rootFile9.mkdirs();
				rootFile = new File(rootFile.getParent() + "/GZH/");
				rootFile.mkdirs();
				rootFile = new File(rootFile.getParent() + "/GZHTEMP/");
				rootFile.mkdirs();
				File rootFile3 = new File(rootFile.getParent() + "/GZHTEMP/1");
				rootFile3.mkdirs();
				File rootFile5 = new File(rootFile.getParent() + "/GZHTEMP/FSN");
				rootFile5.mkdirs();
				File rootFile8 = new File(rootFile.getParent() + "/GZHTEMP/ResidualData");
				rootFile8.mkdirs();
				File rootFile4 = new File(rootFile.getParent() + "/GZHTEMP/2");
				rootFile4.mkdirs();
				File rootFile7 = new File(rootFile.getParent() + "/GZH/PKG2/");
				rootFile7.mkdirs();
			} else
			{
				return;
			}
			File configPath = new File(Constant.configPath);
			if (!configPath.exists())
				configPath.createNewFile();
			File conflog = new File(Constant.configLog);
			if (!conflog.exists())
				conflog.createNewFile();
			File uploadFilePath = new File(Constant.uploadFilePath);
			if (!uploadFilePath.exists())
				uploadFilePath.mkdirs();
			String configPathTemp = "/res/config/fotadata.properties";
			String logPathTemp = "/res/config/log4j.properties";
			InputStream configAsStream = new FotaUtil().getClass().getResourceAsStream(configPathTemp);
			FileOutputStream configOutputStream = null;
			FileOutputStream logOutputStream = null;
			InputStream logAsStream = new FotaUtil().getClass().getResourceAsStream(logPathTemp);
			try
			{
				logOutputStream = new FileOutputStream(conflog);
				configOutputStream = new FileOutputStream(configPath);
				byte[] date = new byte[configAsStream.available()];
				configAsStream.read(date);
				configOutputStream.write(date);
				date = new byte[logAsStream.available()];
				logAsStream.read(date);
				logOutputStream.write(date);
			} catch (Exception e)
			{
				e.printStackTrace();
			} finally
			{
				try
				{
					configAsStream.close();
					configOutputStream.close();
					logAsStream.close();
					logOutputStream.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}

			}
		} catch (Exception e)
		{
			System.out.println();
		}
		Hashtable<String, String> hashtable = new Hashtable<>();
		hashtable.put("log4j.appender.logfile.File", StringHelper.getWINDrive() + "CollectionData/logs/fdas.log");
		hashtable.put("log4j.appender.ErrorFile.File", StringHelper.getWINDrive() + "CollectionData/logs/fdas_error.log");
		PropertyReader.setValueAndStore(Constant.configLog, hashtable);
		hashtable.clear();
		hashtable.put(PropertyConstant.FSNFILEPATH, Constant.uploadFilePath);
		PropertyReader.setValueAndStore(Constant.configPath, hashtable);
	}

	public static void Copy(String oldfile, String newPath)
	{
		FileChannel in = null;
		FileChannel out = null;
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
		try
		{
			File oldf = new File(oldfile);
			inStream = new FileInputStream(oldf);
			File newFile = new File(newPath + "//" + oldf.getName());
			outStream = new FileOutputStream(newFile);
			in = inStream.getChannel();
			out = outStream.getChannel();
			in.transferTo(0, in.size(), out);
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				inStream.close();
				out.close();
				in.close();
				outStream.flush();
				outStream.close();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	// 删除文件和目录
	public static void clearFiles(String workspaceRootPath)
	{
		File file = new File(workspaceRootPath);
		if (file.exists())
		{
			deleteFile(file);
		}
	}

	private static void deleteFile(File file)
	{
		if (file.isDirectory())
		{
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++)
			{
				files[i].delete();
			}
		}

	}

	public static boolean saveCashFile(File file, InputStream inputStream, boolean append)
	{
		OutputStream outputStream = null;
		try
		{
			outputStream = new FileOutputStream(file, append);
			int read = 0;
			byte[] bytes = new byte[10240];

			while ((read = inputStream.read(bytes)) != -1)
			{
				outputStream.write(bytes, 0, read);
			}

		} catch (IOException e)
		{
			e.printStackTrace();

		} finally
		{
			if (outputStream != null)
				try
				{
					outputStream.close();
					inputStream.close();
				} catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return false;
	}

	public static void randomAccessFile(String fileName, byte[] content)
	{
		try
		{
			// 打开一个随机访问文件流，按读写方式
			RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
			// 文件长度，字节数
			long fileLength = randomFile.length();
			// 将写文件指针移到文件尾。
			randomFile.seek(0);
			randomFile.write(content);
			randomFile.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void changeCount2File(String path, int count)
	{
		RandomAccessFile raf = null;
		try
		{
			raf = new RandomAccessFile(path, "rw");
			raf.seek(20);
			;
			raf.write(ByteHelper.int4byte(count));
			System.out.println();
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			if (null != raf)
				try
				{
					raf.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
		}
	}

	public static void changeCRHCount2File(String path, int count)
	{
		RandomAccessFile raf = null;
		try
		{
			raf = new RandomAccessFile(path, "rw");
			raf.seek(15);
			;
			raf.write(ByteHelper.int4byte(count));
			System.out.println();
		} catch (Exception e)
		{
			e.printStackTrace();
		} finally
		{
			if (null != raf)
				try
				{
					raf.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
		}
	}

	public static boolean isNumeric(String str)
	{
		for (int i = 0; i < str.length(); i++)
		{
			System.out.println(str.charAt(i));
			if (!Character.isDigit(str.charAt(i)))
			{
				return false;
			}
		}
		return true;
	}
}
