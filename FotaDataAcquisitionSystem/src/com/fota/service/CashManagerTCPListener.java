package com.fota.service;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import com.fota.service.protocal.FSNCodecFactory;
import com.fota.util.FotaUtil;


public class CashManagerTCPListener {
	static int port = 8234;
	static Logger logger = Logger.getLogger(CashManagerTCPListener.class);
	static IoAcceptor acceptor;

	public static void stop() {
		if (acceptor == null)
			return;
		acceptor.unbind();
		acceptor.dispose();
		logger.info("Server closed. Server version:1");
	}

	public static void start(int tport) {
		port=tport;
		acceptor = new NioSocketAcceptor();
		acceptor.getFilterChain().addLast("logger", new LoggingFilter());
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FSNCodecFactory(false)));
		acceptor.setHandler(new FSNHandler());
		acceptor.getSessionConfig().setReadBufferSize(2048);
		acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 1000);
		try {
			acceptor.bind(new InetSocketAddress(port));
			logger.info("Server started at port:" + port + ". Server version");
		} catch (IOException e) {
			FotaUtil.show("启动服务失败！请检查本机端口！");
			e.printStackTrace();
		}
	}

	public static void restart(int newport) {
		stop();
		port = newport;
		start(port);
	}

	
}
