package com.fota.service.protocal;

import java.nio.ByteOrder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class FSNRequestEncoder extends ProtocolEncoderAdapter  {

	@Override
	public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
			throws Exception {
		FSNRequest request = (FSNRequest)message;
		
		IoBuffer buffer;
		
		if( request.getDataLen() == 0){
			buffer = IoBuffer.allocate(4);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(request.getCode().getBytes());
		}
		else {
			buffer = IoBuffer.allocate(4+4+request.getDataLen());
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(request.getCode().getBytes());
			buffer.putInt(request.getDataLen());
			buffer.put(request.getData());
		}
		buffer.flip();
		out.write(buffer);

	}

}
