package com.fota.service.protocal;

import java.nio.ByteOrder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.fota.util.StringHelper;

public class FSNResponseDecoder extends CumulativeProtocolDecoder {
	private static final String DECODER_STATE_KEY = FSNRequestDecoder.class
			.getName() + ".STATE.PENDING";
	private static final int MAX_DATA_SIZE = 204800;

	@Override
	protected boolean doDecode(IoSession session, IoBuffer in,
			ProtocolDecoderOutput out) throws Exception {
		FSNDecoderState state = (FSNDecoderState) session
				.getAttribute(DECODER_STATE_KEY);
		String code = "";
		in.order(ByteOrder.LITTLE_ENDIAN);
		if (state == null) {
			// this mean new message arrived
			if (in.remaining() >= 4) {
				byte[] buf4 = new byte[4];
				in.get(buf4);
				code = StringHelper.byteArrayToString(buf4, 0, 4);
				if (code.equals(FSNProtocalConstants.RETURN_RESULT_OK)) {
					FSNResponse response = new FSNResponse(code);
					out.write(response);
					return true;
				} else {
					state = new FSNDecoderState();
					state.setCode(code);
					session.setAttribute(DECODER_STATE_KEY, state);
				}
			}
		} else {
			code = state.getCode();
		}
		if (in.prefixedDataAvailable(4, MAX_DATA_SIZE)) {
			int dataLen = 0;
			dataLen = in.getInt();
			byte[] data = new byte[dataLen];
			in.get(data);
			String information = StringHelper.byteArrayToString(data, 0,
					dataLen);
			FSNResponse response = new FSNResponse(code, information);
			out.write(response);
			session.removeAttribute(DECODER_STATE_KEY);
			return true;
		}
		return false;
	}

}
