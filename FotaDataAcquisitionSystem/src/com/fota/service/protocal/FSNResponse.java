package com.fota.service.protocal;

public class FSNResponse {
	private String code;
	private String information;

	public FSNResponse(String code, String information) {
		super();
		this.code = code;
		this.information = information;
	}
	
	public FSNResponse(String code) {
		super();
		this.code = code;
		this.information = "";
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}



}
