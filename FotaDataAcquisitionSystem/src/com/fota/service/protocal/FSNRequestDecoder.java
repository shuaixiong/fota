package com.fota.service.protocal;

import java.nio.ByteOrder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.fota.util.StringHelper;

public class FSNRequestDecoder extends CumulativeProtocolDecoder {

	private static final String DECODER_STATE_KEY = FSNRequestDecoder.class
			.getName() + ".STATE.PENDING";
	static final int DATABUFFER_SIZE = 10 * 1024 * 1024;

	@Override
	protected boolean doDecode(IoSession session, IoBuffer in,
			ProtocolDecoderOutput out) throws Exception {
		in.order(ByteOrder.LITTLE_ENDIAN);
		String code = "";
		FSNDecoderState state = (FSNDecoderState) session
				.getAttribute(DECODER_STATE_KEY);
		if (state == null) {
			// this mean new message arrived
			if (in.remaining() >= 4) {
				byte[] buf4 = new byte[4];
				in.get(buf4);
				code = StringHelper.byteArrayToString(buf4, 0, 4);
				if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_CLOSE)
						|| code.equals(FSNProtocalConstants.MESSAGE_TYPE_CHECK)
						|| code.equals(FSNProtocalConstants.MESSAGE_TYPE_TIMESYNC)) {
					FSNRequest request = new FSNRequest(code);
					out.write(request);
					return true;
				} else {
					state = new FSNDecoderState();
					state.setCode(code);
					state.setDataLenLeft(0);
					session.setAttribute(DECODER_STATE_KEY, state);
				}
			}
		} else {
			code = state.getCode();
		}
		if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE)) {
			int dataLen = 0;
			byte[] data;
			if (state.getDataLenLeft() > 0){
				if(in.remaining() < Math.min(state.getDataLenLeft(), DATABUFFER_SIZE))
				{
					return false;
				}
				dataLen = state.getDataLenLeft();
				if (dataLen < DATABUFFER_SIZE) {
					state.setDataLenLeft(0);
					code = FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_END;
					data = new byte[dataLen];
				} else {
					state.setDataLenLeft(dataLen - DATABUFFER_SIZE);
					dataLen = DATABUFFER_SIZE;
					code = FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCKS;
					data = state.getData();
				}
				in.get(data);
				FSNRequest request = new FSNRequest(code, dataLen, data);
				out.write(request);
				if(state.getDataLenLeft() == 0){
					session.removeAttribute(DECODER_STATE_KEY);
				}
				return true;
			} else if (in.prefixedDataAvailable(4) || in.remaining() >= DATABUFFER_SIZE) {
				dataLen = in.getInt();
				if (dataLen > DATABUFFER_SIZE) {
					state.setDataLenLeft(dataLen - DATABUFFER_SIZE);
					dataLen = DATABUFFER_SIZE;
					code = FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_START;
				}
				data = new byte[dataLen];
				in.get(data);
				FSNRequest request = new FSNRequest(code, dataLen, data);
				out.write(request);
				if(state.getDataLenLeft() == 0){
					session.removeAttribute(DECODER_STATE_KEY);
				}
				else {
					state.setData(data);
				}
				return true;
			}
		} else if (in.prefixedDataAvailable(4)) {
			int dataLen = 0;
			dataLen = in.getInt();
			byte[] data = new byte[dataLen];
			in.get(data);
			FSNRequest request = new FSNRequest(code, dataLen, data);
			out.write(request);
			session.removeAttribute(DECODER_STATE_KEY);
			return true;
		}
		return false;
	}

}
