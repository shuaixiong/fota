package com.fota.service.protocal;

public class FSNDecoderState {
	String code;
	String filename;
	int dataLenLeft;
	byte[] data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void setDataLenLeft(int dataLenLeft) {
		this.dataLenLeft = dataLenLeft;
	}
	
	public int getDataLenLeft() {
		return this.dataLenLeft;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
}
