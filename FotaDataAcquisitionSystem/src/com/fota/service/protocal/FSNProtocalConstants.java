package com.fota.service.protocal;


public class FSNProtocalConstants {
	public static final String MESSAGE_TYPE_CLOSE = "0000";
	public static final String MESSAGE_TYPE_CHECK = "0001";
	public static final String MESSAGE_TYPE_HANDLE = "0002";
	public static final String MESSAGE_TYPE_FOLDER = "0003";
	public static final String MESSAGE_TYPE_FILE = "0004";
	public static final String MESSAGE_TYPE_TIMESYNC = "0005";
	
	public static final String MESSAGE_TYPE_CRHFILE = "0008";//发送CRH文件

	public static final String MESSAGE_TYPE_SERVER_CLOSE = "1000";
	public static final String MESSAGE_TYPE_SERVER_CHECK = "1001";
	public static final String MESSAGE_TYPE_SERVER_HANDLE = "1002";
	public static final String MESSAGE_TYPE_SERVER_FOLDER = "1003";
	public static final String MESSAGE_TYPE_SERVER_FILE = "1004";
	public static final String MESSAGE_TYPE_SERVER_TIMESYNC = "1005";
	
	public static final String MESSAGE_TYPE_FILE_BLOCK_START = "9001";
	public static final String MESSAGE_TYPE_FILE_BLOCKS = "9002";
	public static final String MESSAGE_TYPE_FILE_BLOCK_END = "9003";

	public static final String RETURN_RESULT_OK = "0000";
	public static final String RETURN_RESULT_SERVER_BUSY = "1000";
	public static final String RETURN_RESULT_DATA_ERROR = "1001";
	public static final String RETURN_RESULT_CREATE_FOLDER_FAILED = "1002";
	public static final String RETURN_RESULT_CREATE_FILE_FAILED = "1003";
	public static final String RETURN_RESULT_BUSIINFO_FILE_MISMATCH = "1004";
	public static final String RETURN_RESULT_FILEDATA_MISMATCH = "1005";
	public static final String RETURN_RESULT_RESERVED = "2001";

}
