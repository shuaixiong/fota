package com.fota.service.protocal;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class FSNCodecFactory implements ProtocolCodecFactory
{

	private ProtocolEncoder encoder;
	private ProtocolDecoder decoder;

	public FSNCodecFactory(boolean client)
	{
		if (client)
		{
			encoder = new FSNRequestEncoder();
			decoder = new FSNResponseDecoder();
		} else
		{
			encoder = new FSNResponseEncoder();
			decoder = new FSNRequestDecoder();
		}
	}

	@Override
	public ProtocolDecoder getDecoder(IoSession arg0) throws Exception
	{
		return decoder;
	}

	@Override
	public ProtocolEncoder getEncoder(IoSession arg0) throws Exception
	{
		return encoder;
	}

}
