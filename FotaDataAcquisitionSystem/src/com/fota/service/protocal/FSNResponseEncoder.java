package com.fota.service.protocal;

import java.nio.ByteOrder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import com.fota.util.StringHelper;

public class FSNResponseEncoder extends ProtocolEncoderAdapter  {

	@Override
	public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
			throws Exception {
		FSNResponse response = (FSNResponse)message;	
		String code = response.getCode();
		String information = response.getInformation();
		IoBuffer buffer;
		if( StringHelper.isNullOrEmpty(information)){
			buffer = IoBuffer.allocate(4);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(code.getBytes());
		}
		else {
			buffer = IoBuffer.allocate(4+4+information.length());
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(code.getBytes());
			buffer.putInt(information.length());
			buffer.put(information.getBytes());
		}
		buffer.flip();
		out.write(buffer);
		
	}

}
