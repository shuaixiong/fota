package com.fota.service.protocal;

public class FSNRequest {
	private String code;
	private int dataLen;
	private byte[] data;

	public FSNRequest(String code) {
		super();
		this.code = code;
		this.dataLen = 0;
		this.data = null;
	}
	public FSNRequest(String messageType, int dataLen, byte[] data) {
		super();
		this.code = messageType;
		this.dataLen = dataLen;
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getDataLen() {
		return dataLen;
	}

	public void setDataLen(int dataLen) {
		this.dataLen = dataLen;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
