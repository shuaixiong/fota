package com.fota.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.eclipse.swt.widgets.Display;

import com.fota.service.protocal.FSNProtocalConstants;
import com.fota.service.protocal.FSNRequest;
import com.fota.service.protocal.FSNResponse;
import com.fota.util.BusiInfoFile;
import com.fota.util.CashFileUtil;
import com.fota.util.Constant;
import com.fota.util.FotaUtil;
import com.fota.util.StringHelper;
import com.fota.util.TimeUtil;

public class FSNHandler extends IoHandlerAdapter
{

	static Logger logger = Logger.getLogger(FSNHandler.class);

	private static final String HANDLER_STATE_KEY = FSNHandler.class.getName() + ".STATE.PENDING";

	@Override
	public void sessionCreated(IoSession session) throws Exception
	{
		logger.info("Session created: " + session.getId());
		super.sessionCreated(session);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception
	{
		logger.info("Session opened: " + session.getId());
		super.sessionOpened(session);
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception
	{
		logger.info("Session closed: " + session.getId());
		super.sessionClosed(session);
	}

	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception
	{
		FSNHandlerState state = (FSNHandlerState) session.getAttribute(HANDLER_STATE_KEY);
		if (state != null)
		{
		}
		logger.error("Exception:", cause);
		super.exceptionCaught(session, cause);
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception
	{
		FSNHandlerState state = (FSNHandlerState) session.getAttribute(HANDLER_STATE_KEY);
		if (state == null)
		{
			state = new FSNHandlerState();
			session.setAttribute(HANDLER_STATE_KEY, state);
		}
		FSNRequest request = (FSNRequest) message;
		String code = request.getCode();
		if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_CHECK))
		{
			logger.info("received check message from session: " + session.getId());
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_SERVER_CHECK))
		{
			logger.info("received server check message from session: " + session.getId());
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_SERVER_HANDLE))
		{
			logger.info("received server handle message from session: " + session.getId());
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_HANDLE))
		{
			logger.info("received handle message from session: " + session.getId());
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_TIMESYNC))
		{
			logger.info("received time sync message from session: " + session.getId());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			pushResult(session, FSNProtocalConstants.RETURN_RESULT_OK, sdf.format(new Date()));
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_SERVER_FOLDER))
		{
			logger.info("received server folder message from session: " + session.getId());
			if (request.getDataLen() != 32)
			{
				pushResult(session, FSNProtocalConstants.RETURN_RESULT_DATA_ERROR, "Folder data len <> 32.");
			}
			String date = StringHelper.byteArrayToString(request.getData(), 0, 8);
			String terminalCode = StringHelper.byteArrayToString(request.getData(), 8, 24);
			terminalCode = StringHelper.limitLength(terminalCode, 24, true, '0');
			state.setDate(date);
			state.setTerminalCode(terminalCode);
			state.setFullFolderName(CashFileUtil.createCashFileFolder(date, terminalCode));
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FOLDER))
		{
			/*
			 * 4. 采集服务接收到机具编号，依格式截取地区号、网点号生成目录，目录地址格式为：地区号/网点号/机器编号/日期(yyyymmdd)， 还需要再生成一个唯一的子目录，此子目录用于存放本次冠字号上传的文件。该子目录生成的规则为t_yyyymmddHHMMSSsss。
			 */
			logger.info("received folder message from session: " + session.getId());
			// logger.info("sleep1000.");
			// Thread.sleep(1000);
			if (request.getDataLen() != 32)
			{
				pushResult(session, FSNProtocalConstants.RETURN_RESULT_DATA_ERROR, "Folder data len <> 32.");
			}
			String date = StringHelper.byteArrayToString(request.getData(), 0, 8);
			String terminalCode = StringHelper.byteArrayToString(request.getData(), 8, 24);
			terminalCode = StringHelper.limitLength(terminalCode, 24, true, '0');
			state.setDate(date);
			state.setTerminalCode(terminalCode);
			state.setFullFolderName(CashFileUtil.createCashFileFolder(date, terminalCode));
			pushOkResult(session);
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_SERVER_FILE))
		{
			logger.info("received server file message from session: " + session.getId());
			int filenameLen = request.getData()[0];
			String filename = StringHelper.byteArrayToString(request.getData(), 1, filenameLen);
			logger.info("filename:" + filename);
			state.setPendingFilename(filename);
			String ext = StringHelper.getFileExtention(filename);
			if (ext.equalsIgnoreCase("TSF"))
			{
				state.setCashFilename(filename);
				InputStream cashFileInputStream = new ByteArrayInputStream(request.getData(), 1 + filenameLen, request.getDataLen() - 1 - filenameLen);
				if (CashFileUtil.saveCashFileToFolder(state.getFullFolderName() + "/" + filename, cashFileInputStream, false))
				{
					if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_SERVER_FILE))
					{
						// CashFileUtil.pushTransactionFileParsing(
						// state.getFullFolderName() + "/" + filename);
						// TODO 保存文件
						pushOkResult(session);
					}
				} else
				{
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "create fsn file failed");
				}
				cashFileInputStream.close();
			}
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE) || code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_START))
		{
			logger.info("received file message from session: " + session.getId());
			int filenameLen = request.getData()[0];
			String filename = StringHelper.byteArrayToString(request.getData(), 1, filenameLen);
			//filename = TimeUtil.dateToString(new Date(), TimeUtil.FORMAT_DATE_TIME2) + "_" + filename;
			filename = System.currentTimeMillis() + "_" + filename;
			logger.info("filename:" + filename);
			state.setPendingFilename(filename);
			String ext = StringHelper.getFileExtention(filename);
			String busfileName = state.getFullFolderName() + "/" + filename;
			if (ext.equalsIgnoreCase("FSN"))
			{
				state.setCashFilename(filename);
				InputStream cashFileInputStream = new ByteArrayInputStream(request.getData(), 1 + filenameLen, request.getDataLen() - 1 - filenameLen);
				if (CashFileUtil.saveCashFileToFolder(busfileName, cashFileInputStream, false))
				{
					if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE))
						pushOkResult(session);
				} else
				{
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "create fsn file failed");
				}
				cashFileInputStream.close();
				// } else if (filename.equalsIgnoreCase("busiInfo.txt")) {
			} else if (ext.equalsIgnoreCase("txt"))
			{
				InputStream busiInfoFileInputStream = new ByteArrayInputStream(request.getData(), 1 + filenameLen, request.getDataLen() - 1 - filenameLen);
				if (StringUtils.isEmpty(state.getCashFilename()))
				{
					// error: no cashfile received
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "no fsn file received");
				}
				if (CashFileUtil.saveCashFileToFolder(busfileName, busiInfoFileInputStream, false))
				{
					if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE))
					{
						// CashFileUtil.pushFSNFileParsing(
						// state.getFullFolderName() + "/"
						// + state.getCashFilename(),
						// state.getFullFolderName() + "/" + filename, null);
						// TODO 保存文件
						pushOkResult(session);
						BusiInfoFile busiInfoFile = new BusiInfoFile(busiInfoFileInputStream);
						int recordCount = busiInfoFile.getRecordCount();
						logger.info("busiInfoFile size:" + recordCount);
						for (int i = 0; i < recordCount; i++)
						{
							FotaUtil.showBanknoteWord(busiInfoFile.readNextRecord());
						}
						busiInfoFile.coleStream();
					}
				} else
				{
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "create busiinfo file failed");
				}
				busiInfoFileInputStream.close();
			}
			// else if (ext.equalsIgnoreCase("DAT")) {
			// InputStream cashFileInputStream = new ByteArrayInputStream(
			// request.getData(), 1 + filenameLen,
			// request.getDataLen() - 1 - filenameLen);
			// if (CashFileUtil.parseCashFile(filename, cashFileInputStream)) {
			// pushOkResult(session);
			// } else {
			// pushResult(
			// session,
			// FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED,
			// "error");
			// }
			// cashFileInputStream.close();
			// }
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCKS) || code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_END))
		{
			logger.info("received file blocks from session: " + session.getId());
			String filename = state.getPendingFilename();
			logger.info("filename:" + filename);
			String ext = StringHelper.getFileExtention(filename);
			if (ext.equalsIgnoreCase("FSN"))
			{
				state.setCashFilename(filename);
				InputStream cashFileInputStream = new ByteArrayInputStream(request.getData(), 0, request.getDataLen());
				if (CashFileUtil.saveCashFileToFolder(state.getFullFolderName() + "/" + filename, cashFileInputStream, true))
				{
					if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_END))
						pushOkResult(session);
				} else
				{
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "create fsn file failed");
				}
				cashFileInputStream.close();
				// } else if (filename.equalsIgnoreCase("busiInfo.txt")) {
			} else if (ext.equalsIgnoreCase("txt"))
			{
				InputStream busiInfoFileInputStream = new ByteArrayInputStream(request.getData(), 0, request.getDataLen());
				if (StringUtils.isEmpty(state.getCashFilename()))
				{
					// error: no cashfile received
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "no fsn file received");
				}
				if (CashFileUtil.saveCashFileToFolder(state.getFullFolderName() + "/" + filename, busiInfoFileInputStream, true))
				{
					if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_FILE_BLOCK_END))
					{
						pushOkResult(session);
					}
				} else
				{
					pushResult(session, FSNProtocalConstants.RETURN_RESULT_CREATE_FILE_FAILED, "create busiinfo file failed");
				}
				busiInfoFileInputStream.close();
			}
		} else if (code.equals(FSNProtocalConstants.MESSAGE_TYPE_CLOSE))
		{
			session.close(false);
		}
	}

	protected void pushOkResult(IoSession session)
	{
		pushResult(session, FSNProtocalConstants.RETURN_RESULT_OK, "");
	}

	public static void pushResult(IoSession session, String resultCode, String information)
	{
		FSNResponse response = new FSNResponse(resultCode, information);
		session.write(response);
	}
}
