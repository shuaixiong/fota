package com.fota.service;

import java.io.InputStream;

public class FSNHandlerState {
	String date;
	String terminalCode;
	String cashFilename;
	String fullFolderName;
	String pendingFilename;
//	InputStream cashFileInputStream;
//	InputStream busiInfoFileInputStream;
	

	public String getFullFolderName() {
		return fullFolderName;
	}

	public void setFullFolderName(String fullFolderName) {
		this.fullFolderName = fullFolderName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getCashFilename() {
		return cashFilename;
	}

	public void setCashFilename(String cashFilename) {
		this.cashFilename = cashFilename;
	}

	public String getPendingFilename() {
		return pendingFilename;
	}

	public void setPendingFilename(String pendingFilename) {
		this.pendingFilename = pendingFilename;
	}

//	public InputStream getCashFileInputStream() {
//		return cashFileInputStream;
//	}
//
//	public void setCashFileInputStream(InputStream cashFileInputStream) {
//		this.cashFileInputStream = cashFileInputStream;
//	}
//
//	public InputStream getBusiInfoFileInputStream() {
//		return busiInfoFileInputStream;
//	}
//
//	public void setBusiInfoFileInputStream(InputStream busiInfoFileInputStream) {
//		this.busiInfoFileInputStream = busiInfoFileInputStream;
//	}

}
