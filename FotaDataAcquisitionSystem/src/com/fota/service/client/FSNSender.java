package com.fota.service.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.fota.service.protocal.FSNCodecFactory;
import com.fota.util.StringHelper;

public class FSNSender {
	private static final long CONNECT_TIMEOUT = 5000;
	private static final int RETRY_COUNT = 5;
	static Logger logger = Logger.getLogger(FSNSender.class);

	private static String host = "";
	private static int port = 0;

	public static void sendFSNFile(String fsnFilename, String busiInfoFilename)
			throws InterruptedException {
		if(StringHelper.isNullOrEmpty(host) || port == 0)
		{
			logger.info("parent host not config.");
			return;
		}
		logger.info("sending fsn files:" + fsnFilename + "," + busiInfoFilename);
		NioSocketConnector connector = new NioSocketConnector();
		connector.setConnectTimeoutMillis(CONNECT_TIMEOUT);

		connector.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FSNCodecFactory(true)));
		connector.getFilterChain().addLast("logger", new LoggingFilter());
		SendFSNHandler handler = new SendFSNHandler();
		handler.setSendingFile(fsnFilename, busiInfoFilename);
		connector.setHandler(handler);
		IoSession session = null;

		int retryCount = 0;
		for (;;) {
			try {
				ConnectFuture future = connector.connect(new InetSocketAddress(
						host, port));
				future.awaitUninterruptibly();
				session = future.getSession();
				session.getConfig().setWriteTimeout(20);
				SendFSNHandler.check(session);
				break;
			} catch (RuntimeIoException e) {
				System.err.println("Failed to connect.");
				e.printStackTrace();
				retryCount++;
				if (retryCount >= RETRY_COUNT)
					break;
				Thread.sleep(5000);
			}
		}

		// wait until the summation is done
		if (session != null)
			session.getCloseFuture().awaitUninterruptibly();
		connector.dispose();
	}

	public static void sendTransactionFile(String transactionFilename, String folderString)
			throws InterruptedException {
		if(StringHelper.isNullOrEmpty(host) || port == 0)
		{
			logger.info("parent host not config.");
			return;
		}
		logger.info("sending transaction files:" + transactionFilename);
		NioSocketConnector connector = new NioSocketConnector();
		connector.setConnectTimeoutMillis(CONNECT_TIMEOUT);

		connector.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FSNCodecFactory(true)));
		connector.getFilterChain().addLast("logger", new LoggingFilter());
		SendTransactionFileHandler handler = new SendTransactionFileHandler();
		handler.setSendingFile(transactionFilename, folderString);
		connector.setHandler(handler);
		IoSession session = null;

		int retryCount = 0;
		for (;;) {
			try {
				ConnectFuture future = connector.connect(new InetSocketAddress(
						host, port));
				future.awaitUninterruptibly();
				session = future.getSession();
				session.getConfig().setWriteTimeout(20);
				SendFSNHandler.check(session);
				break;
			} catch (RuntimeIoException e) {
				System.err.println("Failed to connect.");
				e.printStackTrace();
				retryCount++;
				if (retryCount >= RETRY_COUNT)
					break;
				Thread.sleep(5000);
			}
		}

		// wait until the summation is done
		if (session != null)
			session.getCloseFuture().awaitUninterruptibly();
		connector.dispose();
	}

	public static boolean initConfig(boolean fromDatabase) {
		try {
			if(fromDatabase){
				getServerInfo();
			}
			else {
				getServerInfoFromConfig();
			}
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			System.out.println(e.getMessage());
		}
		return false;
	}

	private static void getServerInfo() throws SQLException {
		DataSource ds;
//		ds = CashFileUtil.getDataSource();
//		Connection conn = ds.getConnection();
//		Statement stmt = conn.createStatement();
//		ResultSet rs = stmt
//				.executeQuery("SELECT deploy_value FROM cashmgr.system_deploy WHERE deploy_name = 'parentIP'");
//		while (rs.next()) {
//			host = rs.getString("deploy_value");
//			logger.info("get uplevel server:" + host);
//		}
//		rs = stmt
//				.executeQuery("SELECT deploy_value FROM cashmgr.system_deploy WHERE deploy_name = 'parentport'");
//		while (rs.next()) {
//			String portString = rs.getString("deploy_value");
//			logger.info("get uplevel server port:" + portString);
//			try {
//				port = Integer.parseInt(portString);
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}
//		conn.close();
	}
	
	protected static boolean getServerInfoFromConfig() {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream(System.getProperty("user.dir") + "/conf/client.properties"));
			port = Integer.valueOf(properties
					.getProperty("port"));
			host = properties.getProperty("host");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}


}
