package com.fota.service.client;

import java.net.InetSocketAddress;

import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.fota.service.protocal.FSNCodecFactory;
import com.fota.util.FotaUtil;

public class ConnectServer implements Runnable
{
	private static final long CONNECT_TIMEOUT = 5000;
	private static final int RETRY_COUNT = 1;
	private String host;
	private int port;
	String fsnFilename = "E:/CMG/DataCreator/data/20150622153523/012345.fsn";
	String busiInfoFilename = "E:/CMG/DataCreator/data/20150622153523/012345.txt";

	public ConnectServer(String host, int port, String fsnFilename, String busiInfoFilename)
	{
		super();
		this.host = host;
		
		this.port = port;
		this.busiInfoFilename=busiInfoFilename;
		this.fsnFilename=fsnFilename;
	}

	public void connect() throws InterruptedException
	{
		NioSocketConnector connector = new NioSocketConnector();
		connector.setConnectTimeoutMillis(CONNECT_TIMEOUT);

		connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new FSNCodecFactory(true)));
		connector.getFilterChain().addLast("logger", new LoggingFilter());

		SendFSNHandler sendFSNHandler = new SendFSNHandler();
		sendFSNHandler.setBusiInfoFilename(busiInfoFilename);
		sendFSNHandler.setFsnFilename(fsnFilename);
		connector.setHandler(sendFSNHandler);
		IoSession session = null;

		int retryCount = 0;
		for (;;)
		{
			try
			{
				ConnectFuture future = connector.connect(new InetSocketAddress(host, port));
				future.awaitUninterruptibly();
				session = future.getSession();
				SendFSNHandler.check(session);
				break;
			} catch (RuntimeIoException e)
			{
				System.err.println("Failed to connect.");
				e.printStackTrace();
				retryCount++;
				FotaUtil.show("连接服务器失败！"+retryCount);
				if (retryCount >= RETRY_COUNT)
					break;
				Thread.sleep(5000);
			}
		}

		// wait until the summation is done
		if (session != null)
			session.getCloseFuture().awaitUninterruptibly();
		connector.dispose();
		FotaUtil.cleanOldUploadFile(busiInfoFilename);
		FotaUtil.cleanOldUploadFile(fsnFilename);
		FotaUtil.countDown();
	}

	@Override
	public void run()
	{
		try
		{
			connect();
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args)
	{
//		new Thread(new ConnectServer("192.168.18.22", 8234)).start();
	}

}
