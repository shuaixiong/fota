package com.fota.service.client;

import java.net.InetSocketAddress;

import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.fota.service.protocal.FSNCodecFactory;
import com.fota.util.FotaUtil;

public class ConnectServer2CRH implements Runnable
{
	private static final long CONNECT_TIMEOUT = 5000;
	private static final int RETRY_COUNT = 1;
	private String host;
	private int port;
	String CHRFileName = "";

	public ConnectServer2CRH(String host, int port, String CHRFileName)
	{
		super();
		this.host = host;
		this.port = port;
		this.CHRFileName=CHRFileName;
	}

	public void connect() throws InterruptedException
	{
		NioSocketConnector connector = new NioSocketConnector();
		connector.setConnectTimeoutMillis(CONNECT_TIMEOUT);

		connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new FSNCodecFactory(true)));
		connector.getFilterChain().addLast("logger", new LoggingFilter());

		SendCRHandler sendCRHandler=new SendCRHandler();
		sendCRHandler.setCRHFileName(CHRFileName);
		connector.setHandler(sendCRHandler);
		IoSession session = null;

		int retryCount = 0;
		for (;;)
		{
			try
			{
				ConnectFuture future = connector.connect(new InetSocketAddress(host, port));
				future.awaitUninterruptibly();
				session = future.getSession();
				SendFSNHandler.check(session);
				break;
			} catch (RuntimeIoException e)
			{
				System.err.println("Failed to connect.");
				e.printStackTrace();
				retryCount++;
				FotaUtil.show("连接服务器失败！"+retryCount);
				if (retryCount >= RETRY_COUNT)
					break;
				Thread.sleep(5000);
			}
		}

		// wait until the summation is done
		if (session != null)
			session.getCloseFuture().awaitUninterruptibly();
		connector.dispose();
	}

	@Override
	public void run()
	{
		try
		{
			connect();
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args)
	{
//		new Thread(new ConnectServer2CRH("192.168.18.3", 8234,"E:\\FotaData\\CRH\\123456_00000_0000000002_20150622090000.crh")).start();
	}

}
