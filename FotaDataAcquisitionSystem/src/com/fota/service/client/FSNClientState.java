package com.fota.service.client;

public class FSNClientState {
	int step = 0;

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}
	
	public int nextStep() {
		return ++step;
	}

}
