package com.fota.service.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import com.fota.service.protocal.FSNProtocalConstants;
import com.fota.service.protocal.FSNRequest;
import com.fota.service.protocal.FSNResponse;
import com.fota.util.StringHelper;

public class SendCRHandler extends IoHandlerAdapter
{

	private static final String HANDLER_STATE_KEY = SendCRHandler.class.getName() + ".STATE.PENDING";
	static Logger logger = Logger.getLogger(SendCRHandler.class);

	String CRHFileName = "E:/CMG/DataCreator/data/20150622153523/012345.fsn";

	public void setSendingFile(String CRHFileName)
	{
		this.CRHFileName = CRHFileName;
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception
	{
		FSNClientState state = (FSNClientState) session.getAttribute(HANDLER_STATE_KEY);
		if (state == null)
		{
			state = new FSNClientState();
			session.setAttribute(HANDLER_STATE_KEY, state);
		}
		FSNResponse response = (FSNResponse) message;
		String code = response.getCode();
		if (code.equals(FSNProtocalConstants.RETURN_RESULT_OK))
		{
			doStep(session, state.nextStep());
		}
	}

	public void doStep(IoSession session, int step)
	{
		switch (step)
		{
		case 0:
			check(session);
			break;
		case 1:
			sendFileCRHFile(session, CRHFileName);
			break;
		case 2:
			close(session);
			break;
		default:
			break;
		}
	}

	public static void check(IoSession session)
	{
		FSNRequest request = new FSNRequest(FSNProtocalConstants.MESSAGE_TYPE_CHECK);
		session.write(request);
	}


	public static void sendFileCRHFile(IoSession session, String fullFilename)
	{
		FSNRequest request = new FSNRequest(FSNProtocalConstants.MESSAGE_TYPE_CRHFILE);
		// String foldername =
		// "/Users/shearer/work/Perforce/iShow/Source/Web/CashManager/tests/";
		File file = new File(fullFilename);
		String filename = file.getName();
		int filelen = (int) file.length();
		int dataLen = filelen + 1 + filename.length();
		byte[] data = new byte[dataLen];
		data[0] = (byte) filename.length();
		byte[] filenamedata = filename.getBytes();
		int i = 1;
		for (byte b : filenamedata)
		{
			data[i++] = b;
		}
		InputStream fileStream;
		try
		{
			fileStream = new FileInputStream(file);
			fileStream.read(data, 1 + filename.length(), filelen);
			fileStream.close();
		} catch (FileNotFoundException e)
		{
			logger.error("Exception:", e);
			e.printStackTrace();
		} catch (IOException e)
		{
			logger.error("Exception:", e);
			e.printStackTrace();
		}
		request.setDataLen(dataLen);
		request.setData(data);

		session.write(request);
	}

	public static void close(IoSession session)
	{
		FSNRequest request = new FSNRequest(FSNProtocalConstants.MESSAGE_TYPE_CLOSE);
		session.write(request);
		session.close(false);
	}

	public String getCRHFileName()
	{
		return CRHFileName;
	}

	public void setCRHFileName(String cRHFileName)
	{
		CRHFileName = cRHFileName;
	}
}
