package com.fota.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import com.fota.service.protocal.FSNCodecFactory;


public class CashManagerServer {
	static  int port = 8234;
	static String webRoot = "./";
	static Logger logger = Logger.getLogger(CashManagerServer.class);
	
	private  static void start(int port)
	{
		IoAcceptor acceptor = new NioSocketAcceptor();
		acceptor.getFilterChain().addLast("logger", new LoggingFilter());
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FSNCodecFactory(false)));
		acceptor.setHandler(new FSNHandler());
		acceptor.getSessionConfig().setReadBufferSize(2048);
		acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 1000);
		try {
			acceptor.bind(new InetSocketAddress(port));
			logger.info("Server started at port:"+ port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void restart(int newport) {
		IoAcceptor acceptor = new NioSocketAcceptor();
		acceptor.getFilterChain().addLast("logger", new LoggingFilter());
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FSNCodecFactory(false)));
		acceptor.setHandler(new FSNHandler());
		acceptor.getSessionConfig().setReadBufferSize(2048);
		acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 1000);
		try {
			acceptor.bind(new InetSocketAddress(port));
			logger.info("Server started at port:"+ port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
